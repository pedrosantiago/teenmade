<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model {
    protected $primaryKey = 'service_id';
    protected $table = 'service';
    protected $fillable = array('user_id','position', 'title', 'description', 'color', 'photo', 'views');
    

    
}
