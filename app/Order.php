<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {
    protected $primaryKey = 'order_id';
    protected $table = 'order';
    protected $fillable = array('seller_id', 'user_id', 'address_id', 'total', 'notes', 'status');
    
    public function user(){
	    return $this->belongsTo('App\User');
    }
    
    public function address(){
	    return $this->belongsTo('App\Address');
    }
    
    public function products(){
	    return $this->belongsToMany('App\Product', 'order_product', 'order_id', 'product_id')->withPivot('quantity', 'price','shipping','shipped_at')->withTimestamps();
    }
}
