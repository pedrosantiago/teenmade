<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Project extends Model{
    protected $primaryKey = 'project_id';
    protected $table = 'project';
    protected $fillable = array('user_id','name', 'description', 'photo', 'status');
    
    public function user(){
	    return $this->belongsTo('App\User');
    }
    
    public function company(){
	    return $this->belongsTo('App\Company');
    }
    
    public function phases(){
	    return $this->hasMany('App\Phases');
    }
    
    public function team(){
	    return $this->hasMany('App\Team');
    }
    
}
