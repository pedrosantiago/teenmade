<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Address;


class AddressController extends Controller {
	
	private $messages = [
		'name.required' => 'Fill in the full name of the recipient.',
		'address.required' => 'Type in the complete address.',
		'city.required' => 'You must fill in your city.',
		'state.required' => 'The state is required.',
		'zip_code.required' => 'Zip code is required.'
	];
	
	public function store(Request $request){
		
		$validator = \Validator::make($request->all(), [
			'name' => 'required',
			'address' => 'required',
			'city' => 'required',
			'province' => 'required',
			'zip_code' => 'required'
		], $this->messages);
		
		if($validator->fails())
			return redirect()->back()->withInput()->withErrors($validator)->withModal('create-address');
		
		Address::create([
			'user_id' => \Auth::id(),
			'name' => $request->name,
			'address' => $request->address,
			'city' => $request->city,
			'province' => $request->province,
			'zip_code' => $request->zip_code,
			'phone' => $request->phone
		]);
		
		return redirect()->back()->withNotification('Address created. Now we know where to find you.');
	}
	
	public function destroy(Request $request){
		Address:whereUserId(\Auth::id())->whereAddressId($request->address_id)->delete();
		
		return redirect()->back()->withNotification('Address removed successfully.');
	}
}