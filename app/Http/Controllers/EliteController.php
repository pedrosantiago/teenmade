<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Service;
use App\Product;
use App\Project;
use App\User;
use App\Skill;
use App\Category;

class EliteController extends Controller {
	
	public function index(){
	    return view('elite.index');
    }
    
    public function products(){
	    return view('elite.products', [
		    'products' => Product::all()
	    ]);
    }
    
    public function productsDestroy($product_id){
	    
	    $product = Product::find($product_id);
	    
	    //Use The existend method.
	    ProductController::destroy($product_id);
	    
	    // @todo -> SEND THE E-MAIL.
	    
	    return response()->json([
		    'status' => true,
		    'title' => 'Operation Successfull',
		    'message' => 'Product '.$product->name.' deleted. Photos were deleted but the data is still available.'
	    ]);
	    
    }
    
    public function productsApprove($product_id){
	    
	    $product = Product::find($product_id);
	    $product->is_active = true;
	    $product->save();
	    
	    $user = $product->user;
	    
	     \Mail::send('_email.product_approved', [
		    'user' => $user,
		    'product' => $product,
	    ], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Your product is now Active!');
        });
        
        return redirect()->back()->withNotification('Wohoo! Product '.$product->name.' has been approved. We have notified '.$product->user->name.'.');
	    
	    
    }
    
    public function productsReject(Request $request, $product_id){
	    $product = Product::find($product_id);
	    $product->is_active = 0;
	    $product->save();
	    
	    $user = $product->user;
	    
	    \Mail::send('_email.product_rejected', [
		    'user' => $user,
		    'product' => $product,
		    'reason' => $request->reason
	    ], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Your product was rejected.');
        });
        
        return response()->json([
        	'status' => true,
			'message' => 'Product Rejected. We have sent '.$user->name.' a message with the reason.'
        ]);
	    
    }
    
    public function productsNoteworthy(Request $request){
	    
	    $validator = \Validator::make($request->all(),[
		    'product_id' => 'required',
		    'is_featured' => 'required'
	    ]);
	    
	    if($validator->fails())
	    	return response()->json(['message' => 'Oops, seems like there is something wrong with the request.']);
	    	
	    $product = Product::find($request->product_id);
	    $product->is_featured = $request->is_featured;
	    $product->save();
	    
	    return response()->json(['message' => 'Product '.$product->name.' was updated successfully.']);
	    
    }
    
    public function users(){
	    return view('elite.users', [
		    'users' => User::all()
	    ]);
    }
    
    public function usersDestroy($user_id){
		
		if($user_id == \Auth::id())
			return response()->json([
				'status' => false,
				'title' => 'Stop this madness!',
				'message' => 'You cannot delete yourself'
			]);
			
		$user = User::find($user_id);
		$user->delete();
		
		return response()->json([
			'status' => true,
			'title' => 'Operation Successfull',
			'message' => 'This user is no longer part of teenmade.'
		]);
		
	}
	
	public function usersPasswordRecovery(Request $request, $user_id){
		return app('App\Http\Controllers\UserController')->passwordForgot($request);
	}
	
	public function usersVerify(Request $request, $user_id){
		$user = User::find($user_id);
		$user->is_verified = $request->is_verified;
		$user->save();
		
		
		return response()->json([
			'message' => $request->is_verified ? 'Nice! You just verified '.$user->name.' account :P' : $user->name.'\'s account was just unverified.'
		]);
	}
	
	public function usersSkillsEdit($user_id){
		$user = User::find($user_id);
		return view('user._skills', [
			'skills' => $user->skills,
			'user_id' => $user->user_id
		]);
	}
	
	public function usersSkillsUpdate(Request $request, $user_id){
		
		$fields = $request->all();
		
		$user = User::find($user_id);
		$skills = $user->skills();
		
		foreach($request->skill as $skill_id => $level){
			$skills->updateExistingPivot($skill_id, [
				'level' => $level
			]);
		}
		return redirect()->back()->withNotification('Great! '.$user->name.' skills have been updated ;)');
	}
	
	public function usersSkillsDetach($user_id, $skill_id){
		$user = User::find($user_id);
		$user->skills()->detach($skill_id);
		
		return redirect()->back()->withNotification('Skill removed from '.$user->name.' profile.');
	}
    
    public function skills(){
	    return view('elite.skills', [
		    'skills' => Skill::all()
	    ]);
    }
    
    public function skillsDestroy($skill_id){
	    
	    $skill = Skill::find($skill_id);
	    $skill->delete();
	    
	    return redirect()->back()->withNotification('The skill '.$skill->name.' was successfully removed.');
	    
    }
    
    public function services(){
	    return view('elite.services', [
		    'services' => Service::orderBy('position', 'ASC')->get()
	    ]);
    }
    
	public function servicesStore(Request $request){
		
		$validator = \Validator::make($request->all(), [
			'title' => 'required',
			'description' => 'required',
			'color' => 'required'
		]);
		
		if($validator->fails())
			return redirect()->back()->withModal('create-service')->withInput()->withErrors($validator);
		
		$service = new Service;
		$service->user_id = \Auth::id();
		$service->title = $request->title;
		$service->description = $request->description;
		$service->color = $request->color;
		$service->position = \DB::table('service')->max('position') + 1;
			
		if($request->hasFile('photo')){
			$photo = str_random(20).'jpg';
			\Image::make($request->file('photo'))->fit(650,370)->save('../public/img/other/'.$photo);
			$service->photo = $photo;
		}
		
		$service->save();
		
		return redirect()->back()->withNotification('Great! Now we can also sell '.$request->title.' services');
		
	}
	
	public function servicesPositions(Request $request){
		
		foreach($request->positions as $index=>$service_id){
			Service::find($service_id)->update(['position' => $index]);
		}
		
		return response()->json([
		    'status' => true,
		    'message' => 'Services got its position updated.'
	    ]);
	}
	
	public function servicesDestroy($service_id){
		$service = Service::find($service_id);
		$service->delete();
		
		return redirect()->back()->withNotification(ucfirst($service->title).' services were just removed from TeenMade.');
	}
    
    public function projects(){
	    return view('elite.projects', [
		    'projects' => Project::all()
	    ]);
    }
    
    public function categories(){
	    return view('elite.categories', [
		    'categories' => Category::all()
	    ]);
    }
    
    public function categoriesStore(Request $request){
	    
	    $validator = \Validator::make($request->all(), [
		    'name' => 'required',
		    'slug' => 'required|unique:category'
	    ], [
		    'name.required' => 'Please, fill in the name of the category.',
		    'slug.required' => 'A URL is necessary.',
		    'slug.unique' => 'This URL is already in use, try another.'
	    ]);
	    
	    if($validator->fails())
	    	return redirect()->back()->withModal('create-category')->withInput()->withErrors($validator);
	    
	    Category::create([
		    'name' => $request->name,
		    'slug' => $request->slug
	    ]);
	    
	    return redirect()->back()->withNotification('Great! Category '.$request->name.' inserted successfully.');
    }
    
    public function categoriesDestroy($category_id){    
	    $category = Category::find($category_id);
	    
	    if($category->products->count() > 0)
	    	return response()->json([
		    'status' => false,
		    'title' => 'Bummer',
		    'message' => 'You cannot delete a category with '.$category->products->count().' products inside. Sorry.'
	    ]);
	    
	    $category->delete();
	    return response()->json([
		    'status' => true,
		    'title' => 'Okay.',
		    'message' => 'Great! Category '.$category->name.' deleted.'
	    ]);

    }
}