<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Shop;

class ShopController extends Controller {
    public function index(){
	    return view('admin.shops.index', [
		    'shops' => Shop::all()
	    ]);
    }
}

