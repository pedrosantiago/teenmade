<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Project;

class ProjectController extends Controller {
    public function index(){
	    return view('admin.projects.index', [
		    'projects' => Project::all()
	    ]);
    }
}
