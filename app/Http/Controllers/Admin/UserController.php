<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;

use App\User;


class UserController extends Controller {
    public function login(){
	    
	    if(!Auth::guest() && Auth::user()->is_elite){
		    return redirect('/admin/');
	    }
	    
		return view('admin.login');
	}
    	
	public function authenticate(Request $request){
	   	
	   
	   $this->validate($request, [
           'email' => 'email|required', 
           'password' => 'required',
       ]);
	   	
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->has('remember'))) {
            // Authentication passed... 
            return redirect()->intended('/admin/');
        } else {
	        return redirect('login');
        }
    }
    
    public function index(){
	    return view('admin.users.index', [
		    'users' => User::all()
	    ]);
    }
    
    public function logout(Request $request){
	    Auth::logout();
	    return redirect('/admin/login');
    }
    
}
