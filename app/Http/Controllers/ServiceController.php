<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Service;
use App\Product;
use App\Shop;

class ServiceController extends Controller {
	public function index(){
		return view('service.index', [
			'services' => Service::all()
		]);
	}
	
	public function show($service_id){
		
		$service = Service::find($service_id);
		$service->increment('views');
		
		return view('service.show', [
			'service' => $service
		]);
	}
	
	public function quote(Request $request, $service_id){
		
		$validator = \Validator::make($request->all(), [
			'name' => 'required',
			'email' => 'required|email',
			'company' => 'required',
			'phone' => 'required',
			'description' => 'required'
		], [
			'name.required' => 'We need to know your name.',
			'email.required' => 'The email is required.',
			'company.required' => 'You must specify the company.',
			'phone.required'	=> 'We will need your phone number.',
			'description.required' => 'A short description of the project is necessary.'
		]);
		
		if($validator->fails())
			return redirect()->back()->withErrors($validator)->withInput()->withModal('start-project');
		
		$service = Service::find($service_id);	
		
		\Mail::send('_email.quote', [
			'data' => $request,
			'service' => $service
		], function ($m) use ($service) {

            $m->to('theteam@teenmade.com', 'TeenMade Team')->subject('Project Request - '.$service->title);
        });
		
		return redirect()->back()->withNotification('Awesome! We’ll be in contact in a flash.');
		
	}
	
}