<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Course;
use App\Event;


class CampusController extends Controller {
	public function index(){
		return view('campus.index', [
			'courses' => Course::active()->get(),
			'events' => Event::active()->take(3)->get()
		]);
	}
}