<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Project;

class ProjectController extends Controller {
   	
   	public function index(){	
	   	return view('projects.index', [
		   	'completed' => Project::whereStatus('completed')->take(4)->orderBy('updated_at', 'DESC')->get(),
		   	'featured' => Project::find(2)
	   	]);
   	}	
   	
   	public function show($id){
	   	return view('projects.show', [
		   	'project' => Project::find($id)
	   	]);
   	}
   	
   	public function store(Request $request){
	   	$validator = \Validator::make($request->all(), [
		   	'name' => 'required',
		   	'description' => 'required|max:200|min:30',
		   	'photo' => 'required|image',	   	
	   	]);
	   	
	   	if($validator->fails())
	   		return redirect()->back()->withInput()->withErrors($validator)->withModal('create-project');
	   	
	   	$photo = str_random(20).'.jpg';
		\Image::make($request->file('photo'))->fit(400,400)->save('../public/img/other/'.$photo);
		
		Project::create([
			'user_id' => \Auth::id(),
			'name' => $request->name,
			'description' => $request->description,
			'photo' => $photo
		]);
		
		return redirect()->back()->withNotification('Great! Your project is now added to your profile!');
   	}
   	
   	public function destroy($project_id){
	   	
	   	$project = Project::find($project_id);
	   	
	   	if($project->user_id != \Auth::id() && !\Auth::user()->is_elite)
	   		return redirect()->back()->withNotification('Oops, you are trying to delete a project that is not yours');
	   		
	   	$project->delete();
	   	
	   	return redirect()->back()->withNotification('The project '.$project->name.' was successfully deleted.');
	   	
   	}
}
