<?php 
	
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;

use App\User;
use Auth;
use Validator;
use Mail;


use App\Events\UpdateUserAvatar;
use App\Events\UpdateUserCover;

class UserController extends Controller {
	
	/*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;
    
    /* Password Recovery Methods */
    public function passwordForgot(Request $request){
	   $this->postEmail($request);  
	   return redirect()->back()->withNotification( 'An email with instructions was sent to <strong>'.$request->email.'</strong>. Please, check that out.');
	   
    }

    public function passwordRecover(Request $request, $token){
	    if (is_null($token)) {
            return $this->getEmail();
        }
	    
	    return redirect('/')->withView('user.recover-modal')->withModal('recover')->withToken($token);
    }
    
    public function passwordUpdate(Request $request){
	    
		$validator = Validator::make($request->all(), 
			$this->getResetValidationRules(),
            $this->getResetValidationMessages(), 
            $this->getResetValidationCustomAttributes()
        );
		
		if($validator->fails())
			return redirect()->back()->withView('user.recover-modal')->withModal('recover')->withErrors($validator)->withInput();
	    
        
        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
        
        $broker = $this->getBroker();
        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });
        
        switch ($response) {
            case Password::PASSWORD_RESET:
                return redirect('/')->withNotification([
	                'message' => 'Hi there, your password is now set up and you shall use it to login. Thanks!'
                ]);
            default:
                return redirect('/')->withNotification([
	                'message' => 'Sorry =/'
                ]);
        }
    }
    
	
	
	private $messages = array(
		'name.required' => 'Please, fill your name.',
		'surname.required' => 'Your last name is required.',
		'age.required' => 'We need to know your age.',
		'email.required' => 'Your email is required.',
		'email.unique' => 'This email address is already in use.',
		'password.required' => 'Please, type in your password',
		'confirm_password.required' => 'Confirm your password.',
		'username.required' => 'Please, type in a great username.',
		'username.unique' => 'Sorry, this username already belongs to someone.',
		'avatar.required' => 'Your avatar is required.',
		'cover_picture.required' => 'Please, select a cover photo.',
		'small_bio.required' => 'Just a little bit about you, come on!'
	);
	
	public function create(){
		return view('user.create');
	}
	
	public function store(Request $request){	
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'age' => 'required',
			'email' => 'required|email|unique:user,email',
			'password' => 'required',
			'confirm_password' => 'required|same:password',
		], $this->messages);
		
		if($validator->fails())
			return redirect()->back()->withModal('signup')->withErrors($validator)->withInput();
	
		//Creates the User Account
		$user = User::create([
			'name' => $request->name,
			'email' => $request->email,
			'password' => bcrypt($request->password),
			'age' => $request->age,
			'type' => 'customer'
		]);
		
		Auth::login($user);
		
		if($request->age != '20+'){
			return redirect('me/teen');
		}
		
		return redirect()->back()->withMessage('Welcome!');
	}
	
	/* Boarding Process Methods */
	
	public function teenCreate(){
		return view('user.boarding');
	}
	
	public function teenStore(Request $request){	
		$this->validate($request, [
			'username' => 'required|unique:user',
			'avatar' => 'required',
			'cover_picture' => 'required',
			'small_bio' => 'required'
		], $this->messages);
		
		$user = User::find(\Auth::id());
		
		//This will update the cover following the same criteria for all methods.
		$this->updateCover($request, $user);
		
		//Same thing will happen with avatar
		$this->updateAvatar($request, $user);
		
		$user->username = $request->username;
		$user->small_bio = $request->small_bio;
		$user->type = 'member';
		$user->save();
		
		return redirect('me/shop/create');
	}
	
	public function show($username = null){
		$user = $username == null ? User::find(Auth::id()) : User::whereUsername($username)->first() ; 
		return view('user.show', [
			'user' => $user,
			'shop' => $user->shop,
			'projects' => $user->projects
		]);
	}
	
	//Adds a skill to user.
	public function skill(Request $request){
		
		//Searches for that same skill.
		$skill = \App\Skill::where('name', strtolower($request->skill))->first();	
		
		if($skill == null)
			$skill = \App\Skill::create(['name' => strtolower($request->skill)]);
		
		//Only add this skill to user if he doesn't already have it.
		if(!in_array($skill->name, array_column(\Auth::user()->skills->toArray(), 'name'))){
			$skill->users()->attach(\Auth::id());
		}
		
		return redirect()->back()->withNotification('Skill '.$skill->name.' added into your profile.');
	}
	
	public function edit(){
		$user = User::find(Auth::id());
		return view('user.edit', ['user' => $user]);
	}
	
	public function update(Request $request){
		
		$user = User::find(Auth::id());
		
		$this->validate($request,[
			'name' => 'required',
			'age' => 'required',
			'username' => 'required|unique:user,username,'.$user->user_id.',user_id',
			'email' => 'required|email|unique:user,email,'.$user->user_id.',user_id',
			'password_confirm' => 'required_with:password|same:password'
		], $this->messages);
		
		$user->name = $request->name ;
		$user->email = $request->email;
		$user->age = $request->age;
		
		if(isset($request->password) && $request->password != '')
			$user->password = bcrypt($request->password);
		
		//This events could be added on queue with ease. This should increase the performance.	
		$this->updateCover($request, $user);
		$this->updateAvatar($request, $user);
		
		$user->small_bio = $request->small_bio;
		$user->saving_for = $request->saving_for;
		$user->goal = $request->goal;
		
		$user->save();
		
		return redirect('me')->withNotification('Your profile was updated.');
		
		
	}
	
	public function login(Request $request){   
	   $validator = Validator::make($request->all(), [
           'email' => 'email|required', 
           'password' => 'required',
       ]);
       
       if($validator->fails()){
	       return redirect()->back()->withModal('login')->withErrors($validator)->withInput();
       }
	   	
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->has('remember'))) {
            // Authentication passed... 
            return redirect()->back();
        } else {
	        return redirect()->back()->withModal('login');
        }
    }
    
    public function logout(Request $request){
	    Auth::logout();
	    return redirect('/');
    }
    
    
    public function stripeRedirect(){
	    
	    \Session::put('redirect_back', \URL::previous());
	    
	    return \Socialite::with('stripe')->redirect();
    }
    
    public function stripeConnect(){
	    
	    $user = User::find(\Auth::id());
	    
	    if($user->stripe_user_id != null && $user->stripe_token != null)
	    	return redirect('/');
	    
	    try {
	    	$stripe = \Socialite::with('stripe')->user();
	    } catch(\Exception $e){
		    return redirect('stripe-redirect');
	    }

		$user->stripe_user_id = $stripe->id;
		$user->stripe_token = $stripe->token;
		$user->save();
		
		return redirect(\Session::pull('redirect_back'))->withNotification('Stripe Connected :)');
    }
    
    public function stripeDisconnect(){
	    
	    $user = User::find(\Auth::id());
	    $user->update([
		    'stripe_user_id' => null,
		    'stripe_token' => null
	    ]);
	    
	    return redirect()->back()->withNotification('Okay, we disconected your old Stripe account.');
	    
    }
    
    
    
    private function updateCover(Request $request, User &$user){
	    if($request->hasFile('cover_picture')){
			
			//If the avatar is not null, we must then clean it.
			if($user->cover_picture != null)
			\File::delete('../public/img/cover/'.$user->cover_picture);
			
			$coverName = str_random(20).'.jpg';
			\Image::make($request->file('cover_picture'))->fit(350,200)->save('../public/img/cover/'.$coverName);
			$user->cover_picture = $coverName;
		}
    }
    
    private function updateAvatar(Request $request, User &$user){
	    if($request->hasFile('avatar')){
			
			//If the avatar is not null, we must then clean it.
			if($user->avatar != null)
			\File::delete('../public/img/avatar/'.$user->avatar);
			
			$avatarName = str_random(20).'.jpg';
			\Image::make($request->file('avatar'))->fit(200)->save('../public/img/avatar/'.$avatarName);
			
			$user->avatar = $avatarName;
		}
    }
	
}