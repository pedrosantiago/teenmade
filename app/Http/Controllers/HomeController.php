<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Service;
use App\Product;
use App\Shop;

class HomeController extends Controller {
    public function index(){
	    
	    return view('home', [
		    'services' => Service::orderBy('position')->get(),
		    'featured_products' => Product::active()->featured()->take(6)->get(),
		    'popular_products' => Product::active()->take(6)->get()
	    ]);
    }
    
    public function about(){
	    return view('about');
    }
    
    public function discover(){
	    return view('discover', [
		    'products' => Product::active()->get()
	    ]);
	}

}
