<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;

use \Cart;


class CartController extends Controller {
	public function index(){
		return view('cart.index', [
			'products' => Cart::getContent()
		]);
	}
	
	public function store(Request $request){	
		
		
		
		$product = Product::find($request->product_id);
		
		if(\Auth::check() && \Auth::id() == $product->user_id)
			return redirect()->back()->withNotification('Bummer! You can not add an item of your own to the cart.');
		
		if($product != null){
			Cart::add(array(
			    'id' => $product->product_id,
			    'name' => $product->name,
			    'price' => $product->price,
			    'quantity' => $request->quantity,
			    'attributes' => [
				    'url' => $product->shop->slug.'/'.$product->slug,
				    'shipping' => $product->shipping
			    ]
			));
		}
		
		return redirect('cart')->withNotification("You've added the product <strong>".$product->name."</strong> to your cart.");		
	}
	
	public function update(Request $request, $product_id){
		
		if($request->quantity == 0){
			Cart::remove($product_id);
			return response()->json([
				'message' => 'Product removed!',
				'price' => '0'
			]);
		}
		
		Cart::update($product_id, [
			'quantity' => array(
			  	'relative' => false,
			  	'value' => $request->quantity
			)
		]);
		
		$product = Cart::get($product_id);
		
		$shipping = array_sum(array_pluck(Cart::getContent()->toArray(), 'attributes.shipping'));
		
		return response()->json([
			'message' => 'Product '.$product->name.' quantity updated.',
			'price' => '$'.money($product->getPriceSum() + $product->attributes['shipping']),
			'subtotal' => '$'.money(Cart::getSubTotal()),
			'shipping' => '$'.money($shipping),
			'total' => '$'.money($shipping+Cart::getSubTotal())
		]);
	}
	
	public function destroy(Request $request){
		
		$product = Cart::get($request->product_id);
		
		Cart::remove($request->product_id);

		return redirect('cart')->withNotification("The product <strong>".$product->name."</strong> was removed from your cart.");	
	}
}