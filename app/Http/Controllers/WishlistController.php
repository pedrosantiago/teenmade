<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;

class WishlistController extends Controller {
	
	public function index(){
		return view('wishlist', [
			'user' => User::find(\Auth::id()) 
		]);
	}
	
   	public function store(Request $request){	
	   	
	   	$this->validate($request, [
		   	'product_id' => 'required'
	   	]);
	   	
	   	$this->add($request->product_id);
   	}
   	
   	public function add($product_id){
	   	$user = User::find(\Auth::id());
	   	$user->wishlist()->attach($product_id);
	   	return redirect()->back()->withNotification('Item added to your wishlist!');
   	}
   	
   	public function destroy($product_id){
	   	
	   $user = User::find(\Auth::id());
	   $user->wishlist()->detach($product_id);
	   	
	   	return redirect()->back()->withNotification('Item removed from your wishlist.');
	 
   	}
}
