<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Shop;
use App\Product;
use App\User;

class ShopController extends Controller {
	
	private $messages = [
	   'name.required' => 'Please, name your store!',
	   'logo.required' => 'Your logo is your identity. Please, choose one!',
	   'slug.required' => 'Your shop needs a custom URL',
	   'slug.unique' => 'This URL is already in use =/',
	   'cover_picture.required' => 'Choosing a nice cover picture will increase your sales.'
   ];

	public function index(){
		return view('shop.index', [
			'shops' => Shop::all()
		]);
	}
	
	public function show($slug){
		$shop = Shop::whereSlug($slug)->first();
		if($shop == null)
			return redirect('/');
		
		return view('shop.show', [
			'shop' => $shop
		]);
	}
	
	public function overview(){
		$shop = User::find(\Auth::id())->shop;
		
		//Redirects the user if it has no shop under it's name yet.
		if($shop == null)
			return redirect('shop/create');
		
		$total_views = $shop->products->pluck('views')->sum();
		
		$orders = \App\Order::whereHas('products', function ($query) use ($shop) {
			$query->where('shop_id', '=', $shop->shop_id);
		})->pluck('order_id');
		
		$total_orders = \DB::table('order_product')->whereIn('order_id', $orders)->sum('price');

		return view('shop.overview', [
			'shop' => $shop,
			'products' => $shop->products,
			'total_views' => $total_views,
			'total_orders' => $total_orders
		]);
	}
	
	public function create(){
		return view('shop.create');
	}
	
	public function edit(){
		
		$shop = User::find(\Auth::id())->shop;
		
		//Redirects the user if it has no shop under it's name yet.
		if($shop == null)
			return redirect('shop/create');
		
		return view('shop.edit', [
			'shop' => $shop
		]);
	}
   	
   	public function store(Request $request){	
		$this->validate($request, [
			'name' => 'required',
			'logo' => 'required',
			'slug' => 'required|unique:shop,slug',
			'cover_picture' => 'required'
		], $this->messages);
		
		$shop = new Shop;
		$shop->user_id = \Auth::id();
		$shop->name = $request->name;
		$shop->headline = $request->headline;
		
		$this->updateLogo($request, $shop);
		$this->updateCover($request, $shop);

		$shop->save();
		
		return redirect('me/shop')->withNotification('Your shop <strong>'.$shop->name.'</strong> has been created');
		
   	}	
   	
   	public function update(Request $request){
		$this->validate($request, [
			'name' => 'required',
			'slug' => 'required|unique:shop,slug,'.$request->shop_id.',shop_id'
		], $this->messages);
		
		$shop = Shop::findOrFail($request->shop_id);
		
		$this->updateLogo($request, $shop);
		$this->updateCover($request, $shop);
		
		$shop->name = $request->name;
		$shop->headline = $request->headline;
	    $shop->description = $request->description;
	    $shop->slug = $request->slug;
		//$shop->return_policy = $request->return_policy;
		//$shop->shipping_policy = $request->shipping_policy;
		
		$shop->save();
		
		
		return redirect()->back()->withNotification('Your shop <strong>'.$shop->name.'</strong> has been updated.');
	}
	
	private function updateLogo(Request $request, Shop &$shop){
	    if($request->hasFile('logo')){	
			//If the avatar is not null, we must then clean it.
			if($shop->logo != null)
			\File::delete('../public/img/logo/'.$shop->logo);
			
			$logoName = str_random(20).'.jpg';
			\Image::make($request->file('logo'))->fit(100)->save('../public/img/logo/'.$logoName);
			
			$shop->logo = $logoName;
		}
    }
    
    private function updateCover(Request $request, Shop &$shop){
	    if($request->hasFile('cover_picture')){
		    
			//If the avatar is not null, we must then clean it.
			if($shop->cover_picture != null)
			\File::delete('../public/img/cover/'.$shop->cover_picture);
			
			$coverName = str_random(20).'.jpg';
			\Image::make($request->file('cover_picture'))->fit(1200,380)->save('../public/img/cover/'.$coverName);
			$shop->cover_picture = $coverName;
		}
    }
   	
}
