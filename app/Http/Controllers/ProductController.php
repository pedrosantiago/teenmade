<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Shop;
use App\Product;
use App\Category;

class ProductController extends Controller {
	
	private $messages = [
        'price.required' => 'Please, type the price.',
        'name.required' => 'You must fill the name of the product.',
        'inventory.required' => 'Please fill',
        'slug.required' => 'Custom URL mandatory.',
        'slug.unique' => 'URL already in use =/',
        'description.required' => 'Please, put a description of your product.'
    ];
	
	//Shows all products. But actually it will list the products from user's shop.
	public function index(){
		$shop = Shop::whereUserId(\Auth::id())->first();
		
		return view('product.index', [
			'shop' => $shop,
			'products' => Product::where('shop_id', $shop->shop_id)->with('photos')
		]);
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('product.create', [
	        'categories' => Category::lists('name', 'category_id')
        ]);
    }
    
    public function uploadPhoto(Request $request){
	    
	    $files = [];
	    
	    //We'll save each image for 24 hours in the cache.
		foreach($request->file('files') as $file){
			$photo = str_random(20).'.jpg';
			$file->move(public_path('img/temp'), $photo);
			
			$files[] = array(
				'name' => $file->getClientOriginalName(),
				'size' => $file->getClientSize(),
				'url' => url('/img/large/'.$photo),
				'thumbnailUrl' => url('/img/small/'.$photo),
				'deleteUrl' => url('/me/shop/products/photo/'.$photo),
				'deleteType' => 'DELETE',
				'hash' => $photo
			);
		}
		
		//sleep(10);
		
		return response()->json(['files' => $files]);
		
	}
	
	public function deletePhoto(Request $request, $photo){
		//If it's a product, we'll change the location and remove it also from the database.
		if(isset($request->photo_id)){
			
			\File::delete(public_path('img/product').'/'.$photo);
			\App\Photo::find($request->photo_id)->delete();
		} else {
			\File::delete(public_path('img/temp').'/'.$photo);
		}
		
		return '{"files": [{"'.$photo.'": true}]}';
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        
        $this->validate($request, [
	        'name' => 'required',
	        'price' => 'required',
	        'inventory' => 'required',
	        'slug' => 'required|unique:product,slug',
	        'description' => 'required'
        ], $this->messages);

        
        $shop = Shop::whereUserId(\Auth::id())->first();
        
        $product = Product::create([
	       'user_id' => \Auth::id(),
	       'shop_id' => $shop->shop_id,
	       'category_id' => $request->category_id,
	       'name' => $request->name,
	       'slug' => $request->slug,
	       'price' =>  str_replace(',', '', $request->price),
	       'shipping' => str_replace(',', '', $request->price),
	       'inventory' => $request->inventory,
	       'description' => $request->description,
	       'is_active' => \Auth::user()->is_verified ? 1 : -1
        ]);
        
        if(count($request->photos) > 0){       
	        foreach($request->photos as $photo){
		        $product->photos()->create([
			        'photo' => $photo
		        ]);
		        
		        //Move Files to final destination
		        \File::move(public_path('/img/temp/').$photo, public_path('/img/product').$photo);
	        }
        } 
        
        if(\Auth::user()->is_verified)    
        	return redirect('me/shop')->withNotification('Product '.$request->name.' created. As you are verified, its already online!');
        
        return redirect('me/shop')->withNotification('Product created, but it will appear only after being approved by an Elite Member');
    }
    
    public function show($shop_slug = null, $product_slug = null){
	    if($product_slug == null || $shop_slug == null)
	    	return redirect()->back();
	    
	    //See if the shop exists.	
	    $shop =  Shop::whereSlug($shop_slug)->first();
		if($shop == null)
			return redirect()->back();
		
		//See if the product exists, based on the shop.
	    $product = Product::whereSlug($product_slug)->whereShopId($shop->shop_id)->first();
	    if($product == null)
	    	return redirect()->back();
	    
	    //+1 on views.	
	    $product->increment('views');
	    
	    //Returns the view with the both objects.
	    return view('product.show', [
		    'product' => $product,
		    'shop' => $shop,
	    ]);
	    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
    	return view('product.edit', [
	    	'product' => Product::find($id),
	    	'categories' => Category::lists('name', 'category_id')
    	]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
       	
       	$this->validate($request, [
	        'name' => 'required',
	        'price' => 'required',
	        'inventory' => 'required',
	        'slug' => 'required|unique:product,slug,'.$id.',product_id',
	        'description' => 'required'
        ], $this->messages);
        
        $product = Product::find($id);
        $product->update([
	       'user_id' => \Auth::id(),
	       'category_id' => $request->category_id,
	       'slug' => $request->slug,
	       'name' => $request->name,
	       'price' =>  str_replace(',', '', $request->price),
	       'shipping' => str_replace(',', '', $request->price),
	       'inventory' => $request->inventory,
	       'description' => $request->description,
	       'is_active' => 1
        ]);
        

        
        if(count($request->photos) > 0){
            //OldPhotos 
			$oldPhotos = array_column($product->photos->toArray(), 'photo');
	        
	        foreach($request->photos as $photo){
		        
		        //If we find that the photo is already used, we'll skip processing.
		        if(in_array($photo, $oldPhotos))
		        	continue;
		       
		        $product->photos()->create([
			        'photo' => $photo
		        ]);
		        
		        //Move Files to final destination
		        \File::move(public_path('/img/temp/').$photo, public_path('/img/product/').$photo);
	        }
        }   
        
        return redirect()->back()->withNotification('Product <strong>'.$product->name.'</strong> updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $product = Product::find($id);
        
        if($product->photos->count() > 0){
	        foreach($product->photos as $photo){
		        \File::delete(public_path('img/product/').$photo->photo);
		        $photo->delete();
	        }
        }
        
		$product->delete();
		return redirect('me/shop/products')->withNotification('Product '.$product->name.' deleted.');
    }
}
