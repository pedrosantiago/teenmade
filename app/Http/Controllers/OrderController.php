<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use \Cart;
use \Stripe;


use App\User;
use App\Order;
use App\Product;
use App\Shop;

class OrderController extends Controller {
	
	public function index(){
		
		$shop = Shop::whereUserId(\Auth::id())->first();
		
		return view('order.index', [
			'orders' => Order::with('products', 'address')->whereHas('products', function ($query) use ($shop) {
				$query->where('shop_id', '=', $shop->shop_id);
			})->get()
		]);
	}
	
	// Shows order history for the customer.
	public function history(){
		return view('order.history', [
			'orders' => Order::where('user_id', \Auth::id())->get()
		]);
	}
	
	// Marks an order as shipped.
	public function status($order_id, $product_id){
		$order = Order::find($order_id)->products()->updateExistingPivot($product_id, [
			'shipped_at' => \Carbon::now()
		]);
		
		return redirect()->back()->withNotification('Great! You shipped the product ;)');
	}
	
	public function message(Request $request){
		
		$product = Product::find($request->product_id);
		
		$message = 'Hi '.$product->user->name.', <br>'.$request->name.' has sent you the following message: <br><br>'.nl2br($request->message).'<hr /><br><br>This is about the product '.$product->product_name.' that he bought from you! <br> <strong>Please, answer to the e-mail '.$request->email.'. Thanks.';
		
		\Mail::send('_email.general', ['content' => $message], function ($mail) use ($request, $product) {
            //$m->from($request->email, $request->name);

            $mail->to($product->user->email, $product->user->name);
            $mail->subject('TM #'.$request->order_id.' - '.$request->name.' sent you a message.');
        });
        
        return redirect()->back()->withNotification('Thanks '.$request->name.', we\'ve sent your message. The seller will answer soon!');
	}
	
	//The create order page is actually the checkout. That's a good equivalence.
	public function create(){
		return view('order.create', [
			'user' => User::find(\Auth::id()),
			'products' => Cart::getContent()
		]);
	}
	
	public function store(Request $request){
		
		$user = \Auth::user();
		
		$this->validate($request, [
			'address_id' => 'required',
			'card_name' => 'required',
			'stripe_token' => 'required'
		], [
			'address_id.required' => 'Please, choose an address.',
		]);
		
		\Stripe\Stripe::setApiKey(\Config::get('services.stripe.client_secret'));
		
		
		//We'll need to create a new customer, if it doesn't exists, than we'll create tokens for each Piece of Order
		
		$customer_id = $user->stripe_customer_id;
		if($customer_id == null){
			
			$customer = \Stripe\Customer::create([
				'email' => $user->email,
				'source' => $request->stripe_token,
				'metadata' => [
					'user_id' => $user->user_id
				]
			]);
			
			$customer_id = $customer->id;
				
			$user->stripe_customer_id = $customer_id;
			$user->save();
		} else {
			$customer = \Stripe\Customer::retrieve($customer_id);
			$customer->source = $request->stripe_token;
		}
		
		
		//Then creates the order.
		$order = Order::create([
			'user_id' => $user->user_id,
			'address_id' => $request->address_id,
			'total' => Cart::getTotal() + array_sum(array_pluck(Cart::getContent()->toArray(), 'attributes.shipping')),
			'status' => 'pending' 
		]);
		
		//Proceed to the Payment with Stripe.
		$items = Cart::getContent();
		foreach($items as $item){
			$product = Product::find($item->id);
			
			//Create token for customer.
			\Stripe\Stripe::setApiKey($product->user->stripe_token);

			
			$token = \Stripe\Token::create([
				"customer" => $customer_id,
				 // id of the connected account
			], ['stripe_account' => $product->user->stripe_user_id]);
			
			
			
			//Create the charge
			$finalProductPrice = $item->getPriceSum() + $item->attributes['shipping'];
			$transaction = \Stripe\Charge::create([
				'source' => $token->id,
				'currency' => 'USD',
				'description' => 'TeenMade Order #'.$order->order_id.' - '.$product->name,
				'receipt_email' => $user->email,
				'amount' => $finalProductPrice,
				'application_fee' => $finalProductPrice*0.02
			]);
			
			//Updates the Inventory
			$product->increment('inventory', - $item->quantity);
			
			//Attach product to the order.
			$order->products()->save($product, [
				'quantity' => $item->quantity,
				'price' => $item->price,
				'shipping' => $item->attributes['shipping'],
				'stripe_transaction_id' => $transaction->id
			]);
			
			//Removes the item from cart.
			Cart::remove($item->id);
			
			
		}
		
		return redirect('/orders')->withNotification([
			'Thanks! Your order was registered successfully. The items will be delivered soon.'
		]);
		
	}
}