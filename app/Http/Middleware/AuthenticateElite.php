<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateElite {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null){
	    
        if(!Auth::user()->is_elite){
	        return redirect('/admin/login')->withSwal(json_encode([
				'title' => 'Ops!',
				'text' => Auth::user()->name.', this area is restricted for elite team members. Sorry.'
			]));
        }

        return $next($request);
    }
}
