<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/



Route::group(['middleware' => 'web'], function () {
	
    Route::get('/', 'HomeController@index');
    Route::get('/about', 'HomeController@about');
    
    Route::get('/discover', 'HomeController@discover');
    Route::get('/shops', 'ShopController@index');
    
	
	//Route::resource('campus', 'CampusController');
	
	Route::get('login', 'UserController@loginRedirect');
	Route::post('login', 'UserController@login');
	Route::get('logout', 'UserController@logout');
	
	Route::get('signup', 'UserController@create');
	Route::post('signup', 'UserController@store');
	
	Route::post('forgot-password', 'UserController@passwordForgot');
	Route::get('recover-password/{hash}', 'UserController@passwordRecover');
	Route::post('update-password', 'UserController@passwordUpdate');
	
	// Cart Management.
	Route::get('cart', 'CartController@index');
	Route::post('cart', 'CartController@store');
	Route::post('cart/{product_id}', 'CartController@update');
	Route::delete('/cart', 'CartController@destroy');
	
	Route::get('services', 'ServiceController@index');
	Route::get('services/{service_id}', 'ServiceController@show');
	Route::post('services/{service_id}/quote', 'ServiceController@quote');
	
	Route::group(['middleware' => 'auth'], function(){
		
		Route::get('stripe-redirect', 'UserController@stripeRedirect');
		Route::get('stripe-connect', 'UserController@stripeConnect');
		Route::get('stripe-disconnect', 'UserController@stripeDisconnect');
		
		Route::get('me', 'UserController@show');
		Route::put('me', 'UserController@update');
		
		//Teen Signup (boarding)
		Route::get('me/teen', 'UserController@teenCreate');
		Route::post('me/teen', 'UserController@teenStore');
		
		Route::get('me/edit', 'UserController@edit'); 
		Route::post('me/skill', 'UserController@skill');
				
		//Wishlist Group
		Route::get('wishlist/add/{product_id}', 'WishlistController@add');
		Route::get('wishlist/remove/{product_id}', 'WishlistController@destroy');
		
		Route::resource('wishlist', 'WishlistController');
		
		//Create Projects
		Route::post('projects', 'ProjectController@store');
		Route::delete('projects/{project_id}', 'ProjectController@destroy');
		
		//General Pages
		Route::get('checkout', 'OrderController@create');
		Route::post('checkout', 'OrderController@store');
		
		Route::get('order-history', 'OrderController@history');
		Route::post('message', 'OrderController@message');
		
		Route::resource('address', 'AddressController');
		
		//Shop Group
		Route::get('me/shop', 'ShopController@overview');
		Route::get('me/shop/create', 'ShopController@create');
		Route::post('me/shop', 'ShopController@store');
		Route::get('me/shop/edit', 'ShopController@edit');
		Route::put('me/shop', 'ShopController@update');
		Route::delete('me/shop', 'ShopController@destroy');
		
		//Products Subgroup
		Route::match(['POST', 'PUT'], 'me/shop/products/photo', 'ProductController@uploadPhoto');
		Route::delete('me/shop/products/photo/{photo}', 'ProductController@deletePhoto');
		Route::resource('me/shop/products', 'ProductController');
		
		//Orders Subgroup
		Route::get('me/shop/orders/{order_id}/{product_id}', 'OrderController@status');
		Route::resource('me/shop/orders', 'OrderController');
		
		Route::group(['before' => 'elite'], function(){
			Route::get('elite', function(){ return redirect('elite/dashboard'); });
			Route::get('elite/dashboard', 'EliteController@index');
			
			Route::get('elite/products', 'EliteController@products');
			Route::delete('elite/products/{product_id}', 'EliteController@productsDestroy');
			
			Route::get('elite/products/{product_id}/approve', 'EliteController@productsApprove');
			Route::post('elite/products/{product_id}/reject', 'EliteController@productsReject');
			Route::post('elite/products/noteworthy', 'EliteController@productsNoteworthy');
			
					
			Route::get('elite/users', 'EliteController@users');
			Route::delete('elite/users/{user_id}', 'EliteController@usersDestroy');
			Route::post('elite/users/{user_id}/verify', 'EliteController@usersVerify');
			Route::post('elite/users/{user_id}/password-recovery', 'EliteController@usersPasswordRecovery');
			
			//Subgroup Skills
			Route::get('elite/users/{user_id}/skills', 'EliteController@usersSkillsEdit');
			Route::put('elite/users/{user_id}/skills', 'EliteController@usersSkillsUpdate');
			Route::get('elite/users/{user_id}/skills/{skill_id}', 'EliteController@usersSkillsDetach');
			
			Route::get('elite/services', 'EliteController@services');	
			Route::post('elite/services', 'EliteController@servicesStore');
			Route::post('elite/services/positions', 'EliteController@servicesPositions');
			Route::put('elite/services/{service_id}', 'EliteController@servicesUpdate');
			Route::delete('elite/services/{service_id}', 'EliteController@servicesDestroy');
			
			Route::get('elite/projects', 'EliteController@projects');	
				
			Route::get('elite/categories', 'EliteController@categories');	
			Route::post('elite/categories', 'EliteController@categoriesStore');
			Route::delete('elite/categories/{category_id}', 'EliteController@categoriesDestroy');
			
			Route::get('elite/skills', 'EliteController@skills');	
			Route::delete('elite/skills/{skill_id}', 'EliteController@skillsDestroy');

			//This will be defined 
			
		});


		
    });
    
    Route::get('/user/{username}', 'UserController@show');
    Route::get('/{shop_slug}', 'ShopController@show');
    Route::get('/{shop_slug}/{product_slug}', 'ProductController@show');
    
	
});
