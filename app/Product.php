<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model {
	
	use SoftDeletes;
	
    protected $primaryKey = 'product_id';
    protected $table = 'product';
    protected $fillable = array('user_id', 'shop_id', 'category_id', 'name', 'description', 'price', 'shipping', 'photo', 'is_active', 'details', 'inventory', 'slug');
    
    public function photos(){
	    return $this->hasMany('App\Photo');
    }
    
    public function user(){
	    return $this->belongsTo('App\User');
    }
    
    public function shop(){
	    return $this->belongsTo('App\Shop');
    }
    
    public function orders(){
	    return $this->belongsToMany('App\Order', 'order_product', 'product_id', 'order_id')->withPivot('quantity', 'price','shipping','shipped_at')->withTimestamps();
    }
    
    public function category(){
	    return $this->belongsTo('App\Category');
    }
    
    public function wishlist(){
	    return $this->belongsToMany('App\User', 'wishlist', 'product_id', 'user_id');
    }
    
    public function scopeFeatured($query){
	    return $query->where('is_featured', 1);
    }
    
    public function scopeActive($query){
	    return $query->where('is_active', 1);
    }
}
