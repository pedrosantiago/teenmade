<?php

if (!function_exists('icon')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function icon($icon){
		return sprintf('<i class="mdi mdi-%s"></i> ', $icon);
	}
}
