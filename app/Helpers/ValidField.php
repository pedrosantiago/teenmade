<?php

if (!function_exists('fieldClass')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function field_class($name, $errors){
		if($errors->has($name)){
			return 'validate invalid';
		} 
		
		return 'validate';
		
	}
}
