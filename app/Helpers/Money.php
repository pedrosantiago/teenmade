<?php

if (!function_exists('money')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function money($amt){
		return number_format($amt, 2, '.', ',');
	}
}
