<?php

if (!function_exists('badge')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function badge($icon, $css = null){
	    
	    if($icon == 0)
	    	return '';
	    
	    if($css != null)
	    	$css = sprintf('style="%s"', $css);
	    
		return sprintf('<span class="label label-default label-pill pull-xs-right" %s>%s</span>', $css, $icon);
	}
}
