<?php

if (!function_exists('nav')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function listGroup($items){
	
		$nav = '';

		foreach($items as $key=>$item){	
			$active = '';
			
			if(Request::is($key . '/*') || Request::is($key))
			$active = 'active';
			
			$nav .= sprintf('<a href="%s" class="list-group-item %s">%s</a>', url($key), $active, $item);
			
		}
	
		return $nav;
	}
}
