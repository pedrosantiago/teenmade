<?php

if (!function_exists('nav')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function nav($items){
	
		$nav = '';

		foreach($items as $item){	
			$active = '';
			
			if(Request::is($item['url'] . '/*') || Request::is($item['url']))
			$active = 'active';
			
			$nav .= sprintf('<li class="site-menu-item %s"><a href="%s">', $active, url($item['url']));
			
			if(isset($item['icon']))
	        $nav .= '<i class="site-menu-icon '.$item['icon'].'" aria-hidden="true"></i>'; 
	       
			if(isset($item['title']))
			$nav .= '<span class="site-menu-title">'.$item['title'].'</span>';
			
			$nav .= '</a></li>';
		}
	
		return $nav;
	}
}
