<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Collective\Html\FormBuilder;
use Collective\Html\HtmlBuilder;


class FormServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(){
        $this->registerHtmlBuilder();
        $this->registerFormBuilder();

        $this->app->alias('html', 'Collective\Html\HtmlBuilder');
        $this->app->alias('form', 'Collective\Html\FormBuilder');
    }

    /**
     * Register the HTML builder instance.
     *
     * @return void
     */
    protected function registerHtmlBuilder()
    {
        $this->app->singleton('html', function ($app) {
            return new HtmlBuilder($app['url'], $app['view']);
        });
    }

    /**
     * Register the form builder instance.
     *
     * @return void
     */
    protected function registerFormBuilder()
    {
        $this->app->singleton('form', function ($app) {
            $form = new BootstrapBuilder($app['html'], $app['url'], $app['view'], $app['session.store']->getToken());

            return $form->setSessionStore($app['session.store']);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['html', 'form', 'Collective\Html\HtmlBuilder', 'Collective\Html\FormBuilder'];
    }
}

class BootstrapBuilder extends FormBuilder {
	
	
	public function select($name, $list = [], $selected = null, $options = []){
		$options = array_merge(['class' => 'form-control', 'id' =>  $name], $options);
		return parent::select($name, $list, $selected, $options);
	}
	
	public function text($name, $value = null, $options = []){
		$options = array_merge(['class' => 'form-control', 'id' => $name], $options);
		return parent::text($name, $value, $options);
	}
	
	public function textarea($name, $value = null, $options = []){
		$options = array_merge(['class' => 'form-control', 'id' => $name], $options);
		return parent::textarea($name, $value, $options);
	}
	
	public function password($name, $options = []){
		$options = array_merge(['class' => 'form-control', 'id' => $name], $options);
		return parent::password($name, $options);
	}
}

