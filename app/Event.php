<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Event extends Model{
    protected $primaryKey = 'event_id';
    protected $table = 'event';
    protected $fillable = array('name', 'headline', 'photo', 'is_active');
    
    public function scopeActive(){
	    return $this->where('is_active', true);
    }
    
}
