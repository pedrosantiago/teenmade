<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model {
    protected $primaryKey = 'lesson_id';
    protected $table = 'lesson';
    protected $fillable = array('course_id', 'name', 'video_url', 'position');
    
    public function projects(){
	    $this->hasMany('App\Project');
    }
}
