<?php

namespace App;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable {
	
	use SoftDeletes;
	
	protected $table = 'user' ;
	protected $primaryKey = 'user_id';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'small_bio', 'email', 'age', 'password', 'avatar', 'cover_picture', 'saving_for', 'goal', 'username', 'is_elite', 'is_active', 'type', 'stripe_user_id', 'stripe_token', 'stripe_customer_id', 'is_verified'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];
    
    public function skills(){
	    return $this->belongsToMany('App\Skill', 'user_skill', 'user_id', 'skill_id')->withPivot('level');
    }
    
    public function shop(){
	    return $this->hasOne('App\Shop');
    }
    
    public function teams(){
	    return $this->belongsToMany('App\User', 'team', 'user_id', 'project_id');
    }
    
    public function projects(){
	    return $this->hasMany('App\Project');
    }
    
    public function addresses(){
	    return $this->hasMany('App\Address');
    }
    
    public function wishlist(){
	    return $this->belongsToMany('App\Product', 'wishlist', 'user_id', 'product_id');
    }
}
