<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model {
	
	use SoftDeletes;
	
    protected $primaryKey = 'shop_id';
    protected $table = 'shop';
    protected $fillable = array('user_id', 'name', 'slug', 'logo', 'cover_picture', 'description', 'headline');
    
    public function user(){
	    return $this->belongsTo('App\User');
    }
    
    public function products(){
	    return $this->hasMany('App\Product');
    }
    
    public function totalSalesQuery(){
		return $this->products()->selectRaw('sum(product.price) AS total');
	}
	
	public function getTotalSalesAttribute(){
		if (!array_key_exists('totalSalesQuery', $this->relations)) 
			$this->load('totalSalesQuery');
		
		$related = $this->getRelation('totalSalesQuery')->first();
		unset($this->relations['totalSalesQuery']);
		
		return isset($related) ? (int) $related->total : 0;
	}	
    
}
