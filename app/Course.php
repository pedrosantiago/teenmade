<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Course extends Model{
    protected $primaryKey = 'course_id';
    protected $table = 'course';
    protected $fillable = array('name', 'user_id', 'description', 'photo', 'is_active');
    
    public function scopeActive(){
	    return $this->where('is_active', true);
    }
    
    public function lessons(){
	    return $this->hasMany('App\Lesson');
    }
    
}
