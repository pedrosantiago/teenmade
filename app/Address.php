<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model {
    protected $table = 'address';
    protected $primaryKey = 'address_id';
    
    protected $fillable = ['user_id', 'name', 'company', 'address', 'city', 'province', 'zip_code', 'phone'];
    
    public function user(){
	    return $this->belongsTo('User');
    }
    
}
