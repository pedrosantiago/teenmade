<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model {
    protected $primaryKey = 'phase_id';
    protected $table = 'phase';
    protected $fillable = array('title');
    
    public function projects(){
	    $this->hasMany('App\Project');
    }
}
