<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model {
    protected $primaryKey = 'photo_id';
    protected $table = 'photo';
    protected $fillable = array('product_id', 'photo');
    
    public function product(){
	    $this->belongsTo('App\Product');
    }
}
