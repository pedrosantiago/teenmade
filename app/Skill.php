<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model {
	
	protected $table = 'skill'; 
	protected $primaryKey = 'skill_id';
	
	protected $fillable = ['name', 'badge'];
	
	
	public function users(){
		return $this->belongsToMany('App\User', 'user_skill', 'skill_id', 'user_id');
	}
	
    //
}
