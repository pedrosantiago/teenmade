<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],
    
    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],
    
    'sendgrid' => [
        'api_key' => env('SENDGRID_API_KEY')
    ],

    'stripe' => [
        'model'  			=> App\User::class,
        'client_id'     	=> env('CLIENT_ID', 'ca_8RH6fOSfvxLhUnNfS19s2XZ3rFMSps5J'),
        'client_secret' 	=> env('CLIENT_SECRET', 'sk_test_VqBuMuMuHPDEuSc7xFh4N93d'),
        'redirect' 			=> env('STRIPE_REDIRECT_URI', 'http://localhost/teenmade/public/stripe-connect'),
        'publishable_key' 	=> env('STRIPE_PUBLISHABLE_KEY', 'pk_test_UoLRzRt7ZLvGRXdjKw3GmgkH')
    ],

];
