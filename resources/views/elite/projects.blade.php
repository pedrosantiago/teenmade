@extends('elite.layout')

@section('content-inner')
	<h1>Projects</h1>
	
	@if($projects->count() > 0)
	<table class="table table-hover table-curved">
		<thead>
			<tr>
				<th>Name</th>
				<th>User</th>
				<th>Description</th>
				<th>Created At</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
		@foreach($projects as $project)
			<tr>
				<td>{{ $project->name }}</td>
				<td><a href="{{ url('user',$project->user->username) }}">{{ $project->user->name }}</a></td>
				<td><a data-container="body" data-toggle="popover" data-placement="bottom" data-content="{{ $project->description }}" data-title="Description">{{ str_limit($project->description, 10,'...') }}</a></td>
				<td>{{ Carbon::parse($project->created_at)->diffForHumans() }}</td>
				<td class="actions">
					{{ Form::open(['url' => 'projects/'.$project->project_id, 'method' => 'delete']) }}
					<button type="submit" class="btn btn-secondary btn-sm"><i class="mdi mdi-delete"></i></button>
					{{ Form::close() }}
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
	@endif
	
@endsection