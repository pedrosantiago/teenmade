@extends('elite.layout')

@section('content-inner')
	<h1>Services <a data-toggle="modal" href="#create-service" class="btn pull-right btn-primary">Create Service</a></h1>
	
	@if($services->count() > 0)
	<div class="list-group sortable">
		@foreach($services as $service)
			<a data-id="{{ $service->service_id }}" href="{{ url('services',$service->service_id) }}" class="p-l-0 list-group-item"><button type="button" class="btn pull-left btn-link btn-sm drag text-muted"><i class="mdi mdi-menu"></i></button>{{ $service->title}} <span style="position:relative;top:-4px;" class="label"><i class="mdi mdi-checkbox-blank-circle small" style="color:#{{$service->color }}"></i></span><em class="small text-muted">({{ $service->views }} views)</em>
  	
			{{ Form::open(['url' => 'elite/services/'.$service->service_id, 'method' => 'delete', 'class'=> ' pull-right'])}}
			<button type="submit" class="btn btn-secondary btn-sm drag"><i class="mdi mdi-delete"></i></button>
			{{ Form::close() }}
			</a>
		@endforeach
	</div>
	@endif
	

	
	<!-- Modal -->
	<div class="modal fade" id="create-service" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Create Service</h4>
				</div>
				{{ Form::open(['url' => 'elite/services', 'files' => true]) }}
				<div class="modal-body">
					@include('service._form')
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Create</button>
				</div>
				{{ Form::close() }}
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
@endsection

@section('js-inner')
	<script type="text/javascript" src="{{ asset('js/sortable.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.sortable').sortable({
				items: 'a' ,
				placeholder: '<a href="#" class="list-group-item active">&nbsp;</a>',
			});
			
			$('.sortable').sortable().bind('sortstop', function(e, ui) {
				var sorting = Array();
			   	ui.startparent.children('a').each(function(){
				    sorting[$(this).index()] = $(this).data('id');
			    });
			    
			    $.post('<?php echo url('elite/services/positions'); ?>', {
				    _token: '<?php echo csrf_token();?>',
				    positions: sorting
			    }, function(response){
				    new NotificationFx({
						message : '<i class="mdi mdi-check"></i><p>'+response.message+'</p>',
						layout : 'bar',
						effect : 'slidetop',
						type : 'notice',
					}).show();		
			    });
			});
			
		});
	</script>
@endsection