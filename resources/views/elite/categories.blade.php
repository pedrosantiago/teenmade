@extends('elite.layout')

@section('content-inner')
	<h1>Categories 	<a data-toggle="modal" href="#create-category" class="btn btn-primary pull-right">Create Category</a></h1>
	
	@if($categories->count() > 0)

		@foreach($categories as $category)
			<button  class="btn btn-secondary btn-round m-r-1 ref{{ $category->category_id }}">{{ $category->name }} <a style="color:#666;" class="destroy" data-name="{{ $category->name }}" data-id="{{ $category->category_id }}"> <i class="mdi mdi-close-circle"></i></a></button>
		@endforeach

	@endif
	
	
	<!-- Button trigger modal -->
	
	<!-- Modal -->
	<div class="modal fade" id="create-category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Create Category</h4>
				</div>
				{{ Form::open(['url' => 'elite/categories']) }}
				<div class="modal-body">	
				    <div class="row">
						<fieldset class="form-group col-md-7">
							<label for="name">Name</label>
							{{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'The name of the category.']) }}
							<small class="text-muted">{{ $errors->first('name') }}</small>
						</fieldset>
						<fieldset class="form-group col-md-5">
							<label for="email">URL</label>
							{{ Form::text('slug', null, ['id' => 'slug', 'class' => 'form-control', 'placeholder' => 'The custom URL.']) }}
							<small class="text-muted">{{ $errors->first('slug') }}</small>
						</fieldset>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Create Category</button>
				</div>
				{{ Form::close() }}
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
@endsection

@section('js-inner')
	<script type="text/javascript">
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover();		
			$('.destroy').destroy({
				url: '<?php echo url('elite/categories'); ?>',
				token: '<?php echo csrf_token();?>',
				title: 'Delete the category {name}?',
				text: 'You can only delete categories when they dont have any product'
			});		
		});
	</script>

@endsection
