@extends('layout')

@section('css')
	<link href="{{ asset('css/elite.css') }}" rel="stylesheet">
@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="list-group">
				{!! listGroup([		
					'elite/dashboard' => icon('home').'Dashboard',
					'elite/products' => icon('cube').'Products',
					'elite/users' => icon('account-multiple').'Users ',
					'elite/skills' => icon('cards').'Skills',
					'elite/projects' => icon('flask').'Projects',
					'elite/services' => icon('briefcase').'Services',
					'elite/categories' => icon('format-list-bulleted').'Categories'
				]) !!}

				</div>
			</div>
			<div class="col-md-9">
				@yield('content-inner')
			</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('js/alert.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/switchery.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.switch').each(function(elem){
				
				new Switchery(this, { color: '#039be5' });
			});
			//var init = new Switchery(elem, { color: '#039be5' });
			
			$('[data-toggle="popover"]').popover();
		});
	</script>
	@yield('js-inner')

@endsection