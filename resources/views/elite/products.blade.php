@extends('elite.layout')

@section('content-inner')
	<h1>Products</h1>
	
	<table class="table table-hover table-curved">
		<thead>
			<tr>
				<th>Name</th>
				<th>Seller</th>
				<th>Category</th>
				<th class="text-xs-center">Noteworthy</th>
				<th class="text-xs-center">Status</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
		@foreach($products as $product)
			<tr class="ref{{ $product->product_id }}">
				<td><a href="{{ url($product->shop->slug.'/'.$product->slug) }}">{{ $product->name }}</a></td>
				<td><a href="{{ url($product->shop->slug) }}">{{ $product->shop->name }}</a></td>
				<td>{{ $product->category->name }}</td>
				<td class="text-xs-center">{{ Form::checkbox('noteworthy', $product->product_id,$product->is_featured, ['class' => 'switch']) }}</td>
				<td style="padding-top:9px;text-align:center;">
				
				@if($product->is_active == -1)
				<div class="btn-group" role="group" aria-label="Basic example">
				  <button type="button" class="reject btn btn-sm btn-secondary" data-id="{{ $product->product_id }}"><i class="mdi mdi-thumb-down"></i></button>
				
				  <a href="{{ url('elite/products/'.$product->product_id.'/approve') }}" class="btn btn-sm btn-secondary"><i class="mdi mdi-thumb-up"></i></a>
				</div>
				@else
				<span style="padding-top:3px;display:block;">
					{{ Form::checkbox('status', $product->product_id, $product->is_active, ['class' => 'switch']) }}
				</span>
				@endif
				
				</td>
				<td class="actions">
					<button class="btn btn-secondary btn-sm" data-container="body" data-toggle="popover" data-placement="bottom" data-content="<table class='table table-bordered m-b-0'><tr><th>Views:</th><td>{{ $product->views}}</td></tr><tr><th>Sales:</th><td>{{ $product->orders->count() }}</td></tr></table>" data-html="true" data-title="Description"><i class="mdi mdi-chart-areaspline"></i></button>
					<button type="button" data-name="{{ $product->name }}" data-id="{{ $product->product_id }}" class="destroy btn btn-secondary pull-right btn-sm"><i class="mdi mdi-delete"></i></button>
					
				
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
	
@endsection

@section('js-inner')
	<script type="text/javascript">
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover();		
			$('.destroy').destroy({
				url: '<?php echo url('elite/products'); ?>',
				token: '<?php echo csrf_token(); ?>',
				title: 'Really delete the product {name}?',
				text: 'Its data and orders will be always available. But photos are going to be destroyed.'
			});	
			
			$('.reject').click(function(){
				id = $(this).data('id');
				rejectProduct(id);
			});
			
			$('.switch[name=noteworthy]').on('change', function(e){
				$.post('<?php echo url('elite/products/noteworthy');?>', {
					product_id: $(this).val(),
					is_featured: $(this).is(":checked") ? 1 : 0,
					_token: '<?php echo csrf_token(); ?>'
				}, function(response){			
					new NotificationFx({
						message : '<i class="mdi mdi-check"></i><p>'+response.message+'</p>',
						layout : 'bar',
						effect : 'slidetop',
						type : 'notice',
					}).show();			
				});
			});
			
			$('.switch[name=status]').on('change', function(e){
				if(!$(this).is(":checked")){
					rejectProduct($(this).val())
				} else {
					window.location = '<?php echo url('elite/products');?>/'+$(this).val()+'/approve';
				}
			});
			
			function rejectProduct(id){
				swal({
				  	title: 'Reject Product',
				  	html: '<label>Please, type in the reason:</label><textarea id="reason" name="reason" class="form-control" rows="6"></textarea>',
					buttons: [{
						class: 'btn btn-primary proceed',
						text: 'Proceed',
						event: function(){
							
							$('#reason, .proceed').attr('disabled', true);
							$('.proceed').val('Processing...');
							$.post('<?php echo url('elite/products/'); ?>/'+id+'/reject', {
								reason: $('#reason').val(),
								is_active: 0,
								_token: '<?php echo csrf_token(); ?>'
							}, function(response){
								new NotificationFx({
									message : '<i class="mdi mdi-check"></i><p>'+response.message+'</p>',
									layout : 'bar',
									effect : 'slidetop',
									type : 'notice',
								}).show();
								
								swal.closeModal();
								
								$('#reason, .proceed').attr('disabled', false);
								$('.proceed').val('Proceed');
								$('#reason').val('');
								
							});
						}
					},{
						text: 'Cancel'
					}]
				});
			}

			
			
				
		});
	</script>
@endsection