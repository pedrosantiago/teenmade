@extends('elite.layout')

@section('content-inner')
	<h1>Skills</h1>
	
	<table class="table table-hover table-curved">
		<thead>
			<tr>
				<th>Name</th>
				<th>Users</th>
				<th>Status</th>
				<th>Created At</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
		@foreach($skills as $skill)
			<tr>
				<td>{{ $skill->name }}</td>
				<td>{{ $skill->users->count() }}</td>
				<td>{{ $skill->status }}</td>
				<td>{{ Carbon::parse($skill->created_at)->diffForHumans() }}
				<td class="p-a-0 p-l-1">
					{{ Form::open(['url' => 'elite/skills/'.$skill->skill_id, 'method' => 'delete'])}}
					<button type="submit" class="btn btn-secondary btn-sm" style="margin-top:10px;">{!! icon('delete') !!}</button>
					{{ Form::close() }}	
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
	
@endsection