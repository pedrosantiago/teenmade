@extends('elite.layout')

@section('content-inner')
	<h1>Users</h1>
	
	<table class="table table-hover table-curved">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th class="text-xs-center">Verified</th>
				<th width="170">Actions</th>
			</tr>
		</thead>
		<tbody>
		@foreach($users as $user)
			<tr class="ref{{$user->user_id}}">
				<td><a href="{{ url('user', $user->username) }}">{{ $user->name }}</a> {!! $user->type == 'member' ? '<span class="label label-primary pull-right" style="margin-top:3px;">TEEN</span>' : '<span class="label label-secondary pull-right" style="margin-top:3px;">ADULT</span>' !!}</td>
				<td>{{ $user->email }}</td>

				<td class="text-xs-center">{{ Form::checkbox('verified', $user->user_id, $user->is_verified, ['class' => 'switch']) }}</td>
				<td class="actions">
				{{ Form::open(['url' => 'elite/users/'.$user->user_id.'/password-recovery','class'=> ' pull-left']) }}
					{{ Form::hidden('email', $user->email) }}
					<button type="submit" class="btn btn-sm btn-secondary" href="{{ url('elite/users/'.$user->user_id.'/password-recovery') }}"><i class="mdi mdi-key-change"></i></button>
				{{ Form::close() }}
					
					<button type="button" data-id="{{ $user->user_id }}" class="btn btn-sm btn-secondary pull-left" data-toggle="modal" data-target="#user-skills"><i class="mdi mdi-cards-outline"></i></button>
				

					<button type="button" data-id="{{ $user->user_id }}" data-name="{{ $user->name }}" class="destroy btn btn-secondary pull-right btn-sm"><i class="mdi mdi-delete"></i></button>

				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
<!-- Modal -->
<div class="modal fade" id="user-skills" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			{{ Form::open(['url' => 'elite/users//skills', 'method' => 'put', 'id' => 'skillsForm'])}}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">User Skills</h4>
			</div>

			<div id="skillsHere"></div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save It</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
	
@endsection

@section('js-inner')
	<script type="text/javascript">
		$(document).ready(function(){	
			
			$('#user-skills').on('show.bs.modal', function(event){
				user_id = $(event.relatedTarget).data('id');
				url = '<?php echo url('elite/users');?>/'+user_id+'/skills';
				
				$('#skillsForm').attr('action', url);
				$('#skillsHere').load(url);
			});
			
			$('.destroy').destroy({
				url: '<?php echo url('elite/users'); ?>',
				token: '<?php echo csrf_token(); ?>',
				title: 'Really delete the user {name}?',
				text: 'His shop, products, projects and everything else will be deleted.'
			});
			
			$('.switch[name=verified]').on('change', function(e){
				$.post('<?php echo url('elite/users') ?>/'+$(this).val()+'/verify', {
					is_verified: $(this).is(":checked") ? 1 : 0,
					_token: '<?php echo csrf_token(); ?>'
				}, function(response){			
					new NotificationFx({
						message : '<i class="mdi mdi-check"></i><p>'+response.message+'</p>',
						layout : 'bar',
						effect : 'slidetop',
						type : 'notice',
					}).show();			
				});
			});
					
		});
	</script>
@endsection