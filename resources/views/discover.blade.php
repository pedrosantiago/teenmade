@extends('layout')
@section('content')
	<div class="container">
		<h1>Discover</h1>
		
		<div class="row">
			@each('product._card', $products, 'product')
		</div>
		
	</div>
@endsection