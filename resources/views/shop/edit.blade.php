@extends('layout')

@section('content')

<div class="container">
	<h1>My Store</h1>
	
	@include('shop._menu')
	{{ Form::model($shop, ['url' => 'me/shop', 'method' => 'put', 'files' => true]) }}
		{{ Form::hidden('shop_id', $shop->shop_id ) }}
		@include('shop._form')
		<br><br>
		<button type="submit" class="btn btn-primary btn-lg">Save changes</button>

	{{ Form::close() }}
</div>
@endsection