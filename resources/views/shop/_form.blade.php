<div class="row">
	<fieldset class="form-group col-md-3">
		<label for="name">Name</label>
		{{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'E.g: Brad\'s Italian Store']) }}
		<small class="text-muted">{{ $errors->first('name') }}</small>
	</fieldset>
	<fieldset class="form-group col-md-6">
		<label for="name">Headline</label>
		{{ Form::text('headline', null, ['id' => 'headline', 'class' => 'form-control', 'placeholder' => 'E.g: Best homemade chicken penne.']) }}
		<small class="text-muted">{{ $errors->first('headline') }}</small>
	</fieldset>
	<fieldset class="form-group col-md-3">
	<label for="stripe">Stripe Account</label>
	@if(Auth::user()->stripe_user_id === null)
		<a href="{{ url('stripe-redirect/?') }}" class="btn btn-secondary-outline form-control stripe-connect light-blue"><span>Connect with Stripe</span></a>
	@else 
		<div class="input-group">
	      
	      <input type="text" class="form-control" readonly="readonly" value="{{ Auth::user()->stripe_user_id }}">
	      <span class="input-group-btn">
	        <a href="{{ url('stripe-disconnect') }}" class="btn btn-secondary" type="button">Disconnect</a>
	      </span>
	    </div>
	@endif
	</fieldset>
</div>
<fieldset class="form-group">
	<label for="name">Description</label>
	{{ Form::textarea('description', null, ['id' => 'name', 'class' => 'form-control', 'rows' => '4']) }}
	<small class="text-muted">{{ $errors->first('description') }}</small>
</fieldset>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label for="name">Custom URL</label>
		{{ Form::text('slug', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name of your store.']) }}
		<small class="text-muted">{{ $errors->first('slug') }}</small>
	</fieldset>
	<fieldset class="form-group col-md-4">
		<label for="username">Logo</label>
		{{ Form::file('logo', ['id' => 'logo', 'class' => 'form-control']) }}
		<small class="text-muted">{{ $errors->first('logo') }}</small>
	</fieldset>
	<fieldset class="form-group col-md-4">
		<label for="username">Cover Photo</label>
		{{ Form::file('cover_picture', ['id' => 'cover_picture', 'class' => 'form-control']) }}
		<small class="text-muted">{{ $errors->first('cover_picture') }}</small>
	</fieldset>
</div>


