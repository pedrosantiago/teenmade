@extends('layout')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			@include('shop._card')
		</div>
	</div>
	<br><br>
	<div class="row">
	@each('product._card', $shop->products, 'product')
	</div>
</div>
@endsection