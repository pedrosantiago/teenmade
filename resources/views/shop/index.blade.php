@extends('layout')

@section('content')
	<div class="container">
		<h1>Shops</h1>
		
		<div class="row">
		@foreach($shops as $shop)
			<a class=" deco-none col-md-6" href="{{ url($shop->slug) }}">
				@include('shop._card')
			</a>
		@endforeach
		</div>
		
	</div>
@endsection