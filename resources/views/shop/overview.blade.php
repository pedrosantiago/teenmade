@extends('layout')

@section('content')
<div class="container">
	<h1>My Store</h1>
	
	@include('shop._menu')
	
	<div class="row">
	  <div class="col-sm-6 col-md-4">
	    <div class="statcard statcard-secondary">
	      <div class="p-a-1">
	        <span class="statcard-desc">Page views</span>
	        <h2 class="statcard-number">
	         {{ $total_views }}
	        </h2>

	      </div>

	    </div>
	  </div>
	  
	  <div class="col-sm-6 col-md-4">
	    <div class="statcard statcard-secondary">
	      <div class="p-a-1">
	        <span class="statcard-desc">Sales</span>
	        <h2 class="statcard-number">
	          ${{ money($total_orders) }}
	        </h2>
	        	      </div>
	      
	    </div>
	  </div>
	  
	  <div class="col-sm-6 col-md-4">
	    <div class="statcard statcard-secondary">
	      <div class="p-a-1">
	        <span class="statcard-desc">Products</span>
	        <h2 class="statcard-number">
	          {{ $products->count() }}
	        </h2>
	        	      </div>
	     
	    </div>
	  </div>
	  
	</div>
	<br>
	<hr> 
	<h2>Products <a class="btn btn-primary-outline pull-right m-b-2" href="{{ url('me/shop/products/create') }}">Create Product</a></h2>
		@if($products->count() == 0)
		<div class="jumbotron">
			<h2 class="display-4">Oh no =/ </h2>
			<h3>Seems like you have not created your first product yet!. It's simple, easy and amazingly fast. </h3>
			<a class="btn btn-primary btn-lg " href="{{ url('me/shop/products/create') }}">Get Started</a>
		</div>
	@else
		
		
		@include('product._table')
		
	@endif
	
</div>
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('js/chart.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
var Charts = {
        _HYPHY_REGEX: /-([a-z])/g,
        _cleanAttr: function(t) {
            delete t.chart, delete t.value, delete t.labels
        },
        doughnut: function(element) {
            var attrData = $.extend({}, $(element).data()), data = eval(attrData.value);
            Charts._cleanAttr(attrData);
            var options = $.extend({
                responsive: !0,
                animation: !1,
                segmentStrokeColor: "#fff",
                segmentStrokeWidth: 2,
                percentageInnerCutout: 80
            }, attrData);
            new Chart(element.getContext("2d")).Doughnut(data, options)
        },
        bar: function(element) {
            var attrData = $.extend({}, $(element).data()), data = {
                labels: eval(attrData.labels),
                datasets: eval(attrData.value).map(function(t, e) {
                    return $.extend({
                        fillColor: e%2 ? "#42a5f5": "#1bc98e",
                        strokeColor: "transparent"
                    }, t)
                })
            };
            Charts._cleanAttr(attrData);
            var options = $.extend({
                responsive: !0,
                animation: !1,
                scaleShowVerticalLines: !1,
                scaleOverride: !0,
                scaleSteps: 4,
                scaleStepWidth: 25,
                scaleStartValue: 0,
                barValueSpacing: 10,
                scaleFontColor: "rgba(0,0,0,.4)",
                scaleFontSize: 14,
                scaleLineColor: "rgba(0,0,0,.05)",
                scaleGridLineColor: "rgba(0,0,0,.05)",
                barDatasetSpacing: 2
            }, attrData);
            new Chart(element.getContext("2d")).Bar(data, options)
        },
        line: function(element) {
            var attrData = $.extend({}, $(element).data()), data = {
                labels: eval(attrData.labels),
                datasets: eval(attrData.value).map(function(t) {
                    return $.extend({
                        fillColor: "rgba(66, 165, 245, .2)",
                        strokeColor: "#42a5f5",
                        pointStrokeColor: "#fff"
                    }, t)
                })
            };
            Charts._cleanAttr(attrData);
            var options = $.extend({
                animation: !1,
                responsive: !0,
                bezierCurve: !0,
                bezierCurveTension: .25,
                scaleShowVerticalLines: !1,
                pointDot: !1,
                tooltipTemplate: "<%= value %>",
                scaleOverride: !0,
                scaleSteps: 3,
                scaleStepWidth: 1e3,
                scaleStartValue: 2e3,
                scaleLineColor: "rgba(0,0,0,.05)",
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleFontColor: "rgba(0,0,0,.4)",
                scaleFontSize: 14,
                scaleLabel: function(t) {
                    return t.value.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                }
            }, attrData);
            new Chart(element.getContext("2d")).Line(data, options)
        },
        "spark-line": function(element) {
            var attrData = $.extend({}, $(element).data()), data = {
                labels: eval(attrData.labels),
                datasets: eval(attrData.value).map(function(t) {
                    return $.extend({
                        fillColor: "rgba(255,255,255,.3)",
                        strokeColor: "#fff",
                        pointStrokeColor: "#fff"
                    }, t)
                })
            };
            Charts._cleanAttr(attrData);
            var options = $.extend({
                animation: !1,
                responsive: !0,
                bezierCurve: !0,
                bezierCurveTension: .25,
                showScale: !1,
                pointDotRadius: 0,
                pointDotStrokeWidth: 0,
                pointDot: !1,
                showTooltips: !1
            }, attrData);
            new Chart(element.getContext("2d")).Line(data, options)
        }
    };
	       	$("[data-chart]").each(function() {
	            $(this).is(":visible") && Charts[$(this).attr("data-chart")](this)
	        });
       });
        
	</script>
@endsection