
@extends('layout')

@section('content')
<div class="container">
	<h1>Create Shop</h1>
	{{ Form::open(['url' => 'me/shop', 'files' =>  true, 'style' => 'width:98%;']) }}
		@include('shop._form')
		<br><br>
	<button class="btn btn-lg btn-primary" style="margin-bottom:0;" type="submit" name="action">Create</button>
	<a href="{{ url('me') }}" class="btn btn-link">Just take me to my profile</a>
	{{ Form::close() }}
</div>

@endsection