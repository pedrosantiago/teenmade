<div class="card" >
  <img class="card-img-top img-fluid" src="{{ asset('img/cover/'.$shop->cover_picture) }}" alt="{{ $shop->name }}">
  <div class="card-block">
  	<img class="avatar" src="{{ asset('img/logo/'.$shop->logo) }}" />
  	
    <h4 class="card-title avatar-title" >{{ $shop->name }}</h4>
    <p class="card-text">{{ str_limit($shop->description, 270,'...') }}</p>
  </div>
  @if(isset($show_footer))
  <div class="card-footer">
  	
  	<a class="btn btn-secondary" href="{{ url('me/shop/products/create') }}">Create Product</a>
  	<a class="btn btn-secondary" href="{{ url('me/shop/orders') }}">View Orders</a>
  	<a class="btn btn-secondary" href="{{ url('me/shop/edit') }}">Edit Store</a>
  	<a class="btn btn-secondary pull-right" href="{{ url($shop->slug) }}">{!! icon('eye') !!} </a>
  </div>
  @endif
</div>