@extends('layout')

@section('content')
	<div class="container">
		<h1>Wish List</h1>
		
		@if($user->wishlist->count() > 0)
<table class="table table-curved">
			<thead>
				<tr>
					<th data-field="id">Product Name</th>
					<th data-field="name">Category</th>
					<th data-field="price">Price</th>
					<th data-field="actions">Actions</th>
				</tr>
			</thead>
			<tbody>
            @foreach($user->wishlist as $product)
			<tr>
				<td><a href="{{ url($product->shop->slug.'/'.$product->slug) }}">{{ $product->name }}</a></td>
				<td>{{ $product->category->name }}</td>
				<td>{{ '$'.money($product->price) }}</td>
				<td class="p-a-0">
					{{ Form::open(['url' => 'cart'])}}
						{{ Form::hidden('product_id', $product->product_id) }}
						{{ Form::hidden('quantity', 1) }}
						<button class="btn btn-primary btn-sm pull-left" style="margin-top:9px;margin-left:10px;" type="submit">Add to Cart</button>
					{{ Form::close() }}
					
					{{ Form::open(['url' => 'wishlist/'.$product->product_id, 'method' => 'delete']) }}
						<button style="margin-top:9px;margin-left:5px;" class="btn btn-secondary btn-sm pull-left">Remove</button>
					{{ Form::close() }}
					
				</td>
			</tr>
			@endforeach
			</tbody>
		</table>
		@else
		<div class="jumbotron">
			<h2 class="display-4">No Wishes </h2>
			<h3>Seems like you have not added your first product yet!. It's simple, easy and amazingly fast. </h3>
			<a class="btn btn-primary btn-lg" href="{{ url('discover') }}">Get Started</a>
		</div>
		@endif
		
	</div>
@endsection