@extends('layout')

@section('content')
<div class="main-area">
  

  <div class="container">

  <h3 class="center grey-text" style="margin-bottom: 0;">Learn In Our Campus With</h3>
        <h1 class="relief center project" style="margin-top: 10px;margin-bottom: 20px;">
              <span class="text"></span><span class="cursor">|</span>
             </h1>
        <div class="row" style="margin-bottom: 70px;">
          <div class="col s12 center">
            <a href="submit/" class="btn-large waves-effect waves-light green">Find Mentor</a>
            <a href="" class="btn-large waves-effect waves-light grey lighten-2 grey-text">Become a Mentor</a>
          </div>
        </div>

    <h3 class="col s12">Live Events</h3>
    <div class="row">
		@foreach($events as $event)
		<a href="{{ url('events', $event->event_id) }}" class="col s6 m4 l4">
		  	<div class="panel-small card-panel">
		  		<div class="valign-wrapper">
		  			<img src="{{ asset('img/other/'.$event->photo) }}" alt="" style="padding:5px;">
					<div class="black-text side-ways left">
						<h5 class="truncate" >{{ $event->name }}</h5>
						<span class="stats-project truncate">{{ $event->headline }}</span>
					</div>     
				</div>
	        </div>
		</a>
		@endforeach
    </div>
    
    <!-- END OF LIVE -->
    <h3>Courses</h3>
    @foreach($courses as $course)
    <div class="row">
      <div class="col s12">
        <div class="learn-time">
          <div class="col s5 learn-img" style="background-image:url('{{ asset('img/other/'.$course->photo) }}');"></div>
          <div class="col s7">
            <div class="learn-about">
            <a href="" class="beginning"></a>
              <h3 class="truncate">{{ $course->name }}</h3>
              <p class="learn-by">by {{ $course->author->name }}</p>
              <p>{{ $course->description }}</p>
              <a href="campus/{{ $course->course_id }}" class="btn green z-depth-0">Start Now</a>
            </div>
          </div>
        </div>
      </div>
      @endforeach
      
      
          
        
    </div>
  </div>

</div>
@endsection

@section('js')
<script>
      $(function() {
  var message = {

    message: [ 
      'Mentors',
      'Live Q&As',
      'Video Courses', 
      'Pro Chatter',
      'and More!',
    ],
    counterS: 0,
    counterL: 0,
    deleteS: false,

    init: function() {
      this.cacheElem();
      this.type();
    },

    cacheElem: function() {
      this.$text = $('.text');
    },

    type: function() {
      var message   = this.message[this.counterS],
          that      = this,
          speed     = 0;

      message = !this.deleteS ? message.slice(0, ++this.counterL) : message.slice(0, --this.counterL);
      if(this.message[this.counterS] != message && !this.deleteS) {
        this.$text.text(message);
        speed = 90;
      }
      else {
        this.deleteS = true;
        speed = this.message[this.counterS] == message ? 1500 : 40;
        this.$text.text(message);
        if (message == '') {
          this.deleteS = false;
          this.counterS = this.counterS < this.message.length - 1 ? this.counterS + 1 : 0;
        }
      }
      setTimeout(function(){that.type()}, speed);
    }
  };
  message.init();
});
    </script>

@endsection