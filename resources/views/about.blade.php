@extends('layout')

@section('header')
	<div id="banner">
		<div class="wrapper" style="background-image:url({{ url('img/teenmade-team.jpg') }})"></div>
		<div class="content">
            <h1 class="display-2 cd-headline letters type text-xs-center">
            	<span>We are <strong>TeenMade</strong></span>
            </h1>
		</div>
	</div>
@endsection


@section('content')

<div class="container">

	<h2 style="margin-top:25px;">Our Mission</h2>
	<p class="lead">At Teen Made we aim to be the hub for teens to get their kickstart in the business world. From shops to project, from teen-2-teen course to live Q&As. Everything is focused on teens following their passion and living their dream.</p>
	<p class="lead">We shoot to provide 100% awesome teen made services to individuals and companies. Allowing teens to prove their mad skills to the world, and get incredible experiences on their resume.</p>
	<p class="lead">Providing a nearly complete collection of Teen Made products is the mission at hand. A shop with lower fee's, a friendly rewarding environment, and a supportive community.</p>
	<p class="lead">Creating a place for the world to invest in the future. Where innovation, creativity, determination, and many more incredibly big meaningful words flourish. This is Teen Made and our mission is simply to help our fellow teens.</p>


	<br /><br />	

    <div class="row">
      <div class="col col-md-4">
        <img src="{{ asset('/img/avatar/alex.png') }}" class="img-thumbnail" alt="Alex Elite Teen Made">
      </div>
      <div class="col col-md-4">
        <img src="{{ asset('/img/avatar/sterling.png') }}" class="img-thumbnail" alt="Sterling Elite Teen Made">
      </div>
      <div class="col col-md-4">
        <img src="{{ asset('/img/avatar/conner.png') }}" class="img-thumbnail" alt="Conner Elite Teen Made">
      </div>
	</div>
	<br><br>
	
	<h2>The Elite Team</h2>

    <p class="lead">All three of us Alex (13), Sterling (17), and Conner (15) make up the small and mighty Teen Made Elite team. One of us is good at robotics, another graphics, another coding. . . One likes lacrosse, one of us like dancing, and the other soccer. We have so many different interest, and as more join the Elite Team I'm sure there will be even more diversity.</p>
    <p class="lead">Through this diversity we have been able to combine our talents and work toward something we all believe in. We have joined forces to help our fellow teens follow their passion, and get their start in the business world.</p>
    <p class="lead">The Teen Made Elite Team is the top skill ranking at Teen Made. Elite Team Member work together to keep Teen Made running smoothly. From creating course to managing project, from social engagement to product enhancement, Teen Made Elite Members grow together.</p>
    <p class="lead">If you have talent you want to bring to the Teen Made Elite team, and have a passion for helping your fellow teens, we would be more than happy to hear from you. <a href="#!">Join the Elite Team</a></p>

</div>


@endsection