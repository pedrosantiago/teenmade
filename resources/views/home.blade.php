@extends('layout', ['menu' => 'transform'])

@section('header')
	<div id="banner">
		<div class="wrapper"></div>
		<div class="content">
            <h1 class="display-4 cd-headline letters type text-xs-center">
            	<span>100% Teen Made </span>
            	<span class="cd-words-wrapper waiting">
            		<b class="is-visible">web design.</b>
            		<b>apparel.</b>
            		<b>photography.</b>
            	</span>
            </h1>

            <h5 class="text-uppercase text-xs-center">Buy teen products & hire professional teen services</h5>
            <div id="buttons">
            </div>

		</div>
	</div>
@endsection

@section('content')
	<section class="bg-light p-a-3">
		<div class="container">	
			<div class="row">
				<div class="col-md-8">
					<h2>TeenMade is the HUB. </h2>
					<p class="lead">For the world to invest in the future (A.K.A. teens). Where anyone can purchase 100% Teen Made products, hire professional teens for services, and mentor teens to help them on their way. The place teens can dive into projects, create products, and gather a repertoire of skills in the campus.</p>
				  
					<nav class="nav nav-inline nav-lg">
						<a href="{{ url('projects') }}" class="nav-link active">Start Your Project</a>
						<a href="{{ url('campus') }}" class="nav-link">Develop New Skills</a>
						<a href="{{ url('about') }}" class="nav-link">Meet the Team</a>
						<a href="{{ url('press') }}" class="nav-link">Press</a>
					</nav>
				</div>
				
				<div class="col-md-4">
					<div class="card card-block">
					  	<h4 class="card-title">Latest News</h4>
						<blockquote class="blockquote">
							<p class="m-b-0">Balancing the Madness: 10 Tips to Organize Life as a Young Entrepreneur</p>
							<footer class="blockquote-footer">Someone famous in <cite title="Source Title">TeenMade</cite></footer>
						</blockquote>
					</div>
				</div>
								
			</div>
		</div>
	</section>
	<section id="content" class="container p-t-3 p-l-1">
		<h2>New & Noteworthy</h2>	
		<div class="row">
			@each('product._card', $featured_products, 'product')
		</div>
		<br />
		<br />
		
		
		<h2>Featured Services</h2>
		<!-- Services -->
		<div class="row slider">
			@each('service._card', $services, 'service')
		</div>
		
		<br><br>

		<h2>Popular Products <i class="fa fa-deaf" style="color:black;"></i></h2>	
		<div class="row ">
			@each('product._card', $popular_products, 'product')
		</div>
		
			
	</section>
@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function(){
		$(".slider").slick({
	        dots: false,
	        infinite: true,
	        slidesToShow: 3,
	        slidesToScroll: 1
	    });
	});
</script>
@endsection
