@extends('layout')

@section('content')
<div class="main-area">
	<div class="container">
		<div class="row no-margin">
			<div class="col s12">
				<div class="project-time-header ">
					<div class="parallax-overlay">
						<div class="parallax-container">
				      		<div class="parallax"><img src="{{ asset('img/cover/'.$project->cover) }}" alt="" style="display: block; transform: translate3d(-50%, 219px, 0px);"></div>
				      		<div class="fp-table">
				      			<div class="fp-tableCell">
				      			<h3 class="white-text center-align ">{{ $project->name }}</h3>
				      			<p class="center-align">{{ $project->company_id != null ? $project->company->name : '' }}</p>
				      			</div>
				      		</div>
				    	</div>
					</div>
					<div class="progress">
                  <div class="determinate" style="width: 70%"></div>
                </div>
			    	<ul class="tabs" style="width: 100%;">
				     	<li class="tab col s3"><a class="active" href="#test3">Feed</a></li>
				        <li class="tab col s3"><a href="#about-project">About</a></li>
				        <li class="tab col s3"><a href="#team">Team </a></li>
			      	<div class="indicator" style="right: 665px; left: 0px;"></div></ul>
		      </div>
			</div>
		</div>
		<!-- <div class="row ">
		    <div class="col s12">
		      
		    </div>
  		</div> -->
  		<div class="row">
  			<div class="col s12">
  				<div class="project-area">
  					
  					<!-- ABOUT TAB -->
  					<div id="about-project" class="col s12" style="display: none;">
						<div class="row">
							<div class="card-panel white">
								<div class="about-header">
									<h3>About</h3>
								</div>
								<div class="card-panel-content">
									{{ $project->description }}
								</div>
							</div>
							@foreach($project->phases as $phase)
							<div class="card-panel white">
								<div class="about-header">
									<h3>{{ $phase->name }}</h3>
								</div>
								<div class="card-panel-content">
									<p>{{ $phase->description }}</p>
								</div>
							</div>
							@endforeach
						</div>
  					</div>

  					<!-- TEAM TAB -->
		    		<div id="team" class="col s12 project-team" style="display: none;">
			    		
			    		<div class="row">
			    			<h3>Open Positions</h3>

			    			<div class="row team-members team2">
								<div class="col s12 m6 l4">
					                <div class="panel-small card-panel">
					                  <div class="valign-wrapper">
					                      <div src="" alt="" class="circle skill-level red">
					                      	<span class="words">Pro</span>
					                      </div>
					             
					                      <div class="black-text side-ways left">
					                        <h5 class="truncate">Graphic Designer</h5>
					                        <span class="stats-project truncate">Paying Position · <a href="#modal1" class="modal-trigger">Details</a></span>
					                      </div>     
					                  </div>
					                </div>
				                </div>
				                <div class="col s12 m6 l4">
					                <div class="panel-small card-panel">
					                  <div class="valign-wrapper">
					                      <div src="" alt="" class="circle skill-level orange lighten-2">
					                      	<span class="words">Int</span>
					                      </div>
					             
					                      <div class="black-text side-ways left">
					                        <h5 class="truncate">Graphic Designer</h5>
					                        <span class="stats-project truncate">Paying Position · <a href="#modal1" class="modal-trigger">Details</a></span>
					                      </div>     
					                  </div>
					                </div>
				                </div>
				                <div class="col s12 m6 l4">
					                <div class="panel-small card-panel">
					                  <div class="valign-wrapper">
					                      <div src="" alt="" class="circle skill-level green lighten-1">
					                      	<span class="words">Beg</span>
					                      </div>
					             
					                      <div class="black-text side-ways left">
					                        <h5 class="truncate">Graphic Designer</h5>
					                        <span class="stats-project truncate">Portfolio Building · <a href="#modal1" class="modal-trigger">Details</a></span>
					                      </div>     
					                  </div>
					                </div>
				                </div>
							</div>

			    			<!-- <div class="row team-position">
								<div href="#!" class="col s12">
					                <div class="panel-small card-panel">
					                  <div class="valign-wrapper">
					                      <img src="../pics/sterling3.png" alt="" style="padding:5px;" class="circle">
					             
					                      <div class="black-text side-ways left">
					                        <h5 class="truncate">Web Designer</h5>
					                        <span class="stats-project truncate">Pro Skills &middot; 24 Submissions</span>

					                      </div>  
					                      <a href="" class="btn  green z-depth-0 right">Submit</a>   
					                  </div>
					                </div>
				                </div>
							</div> -->
			    			<!-- <ul class="collapsible z-depth-0 team-positions popout" data-collapsible="accordion">
							    <li>
							      <div class="collapsible-header"><i class="material-icons">filter_drama</i>First <a href="" class="btn small green z-depth-0 right">Join</a></div>
							      <div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
							    </li>
							    <li>
							      <div class="collapsible-header"><i class="material-icons">place</i>Second</div>
							      <div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
							    </li>
							    <li>
							      <div class="collapsible-header"><i class="material-icons">whatshot</i>Third</div>
							      <div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
							    </li>
						  	</ul> -->
						</div>
						<div class="row">
							<h3>Team Members</h3>
							<div class="row">
							<div class="col s12 m6 l4">
							<div class="profile-card">
    <!-- Header -->
    <div class="profile-card-header">
      <div class="profile-card-header__avatar" style="background-image: url(http://s3-us-west-2.amazonaws.com/s.cdpn.io/169963/profile/profile-512_16.jpg);"></div>
      <div class="profile-card-header__follow">Pro</div>
    </div>
    <!-- Content-->
    <div class="profile-card-content">
      <a href="#!" class="profile-card-content__username">Andy Tran<!-- <span class="badge">PRO</span> --></a>
      <div class="profile-card-content__bio">Graphic Designer</div>
    </div>
    <!-- Footer-->
    <!-- <div class="profile-card-footer">
      <div class="profile-card-footer__pens"> <span>15</span>
        <div class="label">Pens</div>
      </div>
      <div class="profile-card-footer__followers"> <span>582</span>
        <div class="label">Followers</div>
      </div>
      <div class="profile-card-footer__following"> <span>28</span>
        <div class="label">Following</div>
      </div>
    </div> -->
  </div>

  </div>
  </div>
						</div>
		    		</div>

		    		<!-- FEED TAB -->
		    		<div id="test3" class="col s12">


		    		<ul class="postit">
  <li>
    <div class="post-icon"><img src="../pics/alex.png" alt=""></div>
    <div class="post-content">
    <div class="post-header"><a href="#!"><span class="full-name">Alex</span> <span class="account-name">@alexsafayan</span></a> · 12m ago</div>
    <p>Hey I finished the Tour Date design for the Shows page and synced it to the Git <a href="">@ItsMeSterling</a>. . . Let me know if there is anything that we need to change. </p>
    </div>
    <div class="post-footer">
      <div class="post-footer-content"> 

        <a href="" class="commentit"><i class="mdi-communication-forum"></i><span class="post-num">9</span></a> 
        <a href="" class="shareit active"><i class="mdi-content-reply"></i><span class="post-num">4</span></a>
        <a href="" class="starit active"><i class="mdi-action-grade"></i><span class="post-num">32k</span></a>
        
        
      </div>
    </div>
  </li>

  <li>
    <div class="post-icon"><img src="../pics/sterling.png" alt=""></div>
    <div class="post-content">
    <div class="post-header"><a href="#!"><span class="full-name">Sterling</span> <span class="account-name">@ItsMeSterling</span></a> · August 20</div>
    <p><a href="">#task</a> <a href="">@alexsafayan</a> Hey I need you to do the Tour Date design. Red Outline buttons, and when hover fill. . . Then we are using the Raleway font. Message me if you have any questions. </p>
    </div>
    <div class="post-footer">
      <div class="post-footer-content"> 

        <a href="" class="commentit"><i class="material-icons">communication_forum</i><span class="post-num">11</span></a> 
        <a href="" class="shareit"><i class="material-icons">content_reply</i><span class="post-num"></span></a>
        <a href="" class="starit "><i class="material-icons">action_grade</i><span class="post-num">13</span></a>
        
        
      </div>
    </div>
  </li>

  <li>
    <div class="post-icon"><img src="../pics/hannah.png" alt=""></div>
    <div class="post-content">
    <div class="post-header"><a href="#!"><span class="full-name">Hannah</span> <span class="account-name">@Hannah</span></a> · Jan 5</div>
    <p>Working on pulling together the information for the Polling system. . . I'll send that over to you right now <a href="">@ItsMeSterling</a></p>
    </div>
    <div class="post-footer">
      <div class="post-footer-content"> 

        <a href="" class="commentit"><i class="mdi-communication-forum"></i><span class="post-num">11</span></a> 
        <a href="" class="shareit active"><i class="mdi-content-reply"></i><span class="post-num">7</span></a>
        <a href="" class="starit active"><i class="mdi-action-grade"></i><span class="post-num">3</span></a>
        
        
      </div>
    </div>
  </li>

  <li>
    <div class="post-icon"><img src="../pics/alex.png" alt=""></div>
    <div class="post-content">
    <div class="post-header"><a href="#!"><span class="full-name">Alex</span> <span class="account-name">@alexsafayan</span></a> · Jan 5</div>
    <p>Just joined the project. . . Excited to get started!!!</p>
    </div>
    <div class="post-footer">
      <div class="post-footer-content"> 

        <a href="" class="commentit"><i class="mdi-communication-forum"></i><span class="post-num">134</span></a> 
        <a href="" class="shareit"><i class="mdi-content-reply"></i><span class="post-num">20</span></a>
        <a href="" class="starit active"><i class="mdi-action-grade"></i><span class="post-num">749</span></a>
        
        
      </div>
    </div>
  </li>
  

  

  
  <li class="noMore"><div class="post-icon"><i class="mdi-content-add"></i></div>Yeah You Have Reached the End!</li>
</ul>


		    		</div>
  					<div class="huge"></div>
  				</div>
  			</div>
  		</div>
	</div>

</div>
@endsection