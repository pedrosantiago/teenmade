@extends('layout')

@section('content')
<div class="main-area">
	<div class="container">
		<h1 class="center grey-text" style="margin-bottom: 0;">Top Services</h1>
        <h1 class="relief center project" style="margin-top: 10px;margin-bottom: 20px;">
              <span class="text"></span><span class="cursor">|</span>
        </h1>
		<div class="row" style="margin-bottom: 70px;">
			<div class="col s12 center">
				<a href="submit/" class="btn-large waves-effect waves-light green">See All Services</a>
				<a href="" class="btn-large waves-effect waves-light grey lighten-2 grey-text">Join Project Team</a>
			</div>
		</div>

        <h3>Recently Completed</h3>

		<div class="row">
		@foreach($completed as $project)
		
			<div class="col s12 m6 l4">
				<a href="{{ url('projects', $project->project_id) }}" class="tile"><img src="{{ $project->photo }}"/>
					<div class="details">
						<span class="title">{{ $project->name }}</span>
						<span class="info">{{ $project->company_id != null ? $project->company->name : '' }}</span>
					</div>
				</a>
			</div>
		
		@endforeach

        </div>

      
        <h3>Featured Project</h3>
        
        <div class="row">
			<div class="col s12">
				<div class="learn-time">
					<a href="{{ url('projects',$featured->project_id) }}" class="col s5 learn-img waves-effect waves-block waves-light" style="background-image:url('../pics/bt.png');"></a>
					<div class="col s7">
						<div class="learn-about">
							<a href="#!" class="beginning"></a>
							<h3 class="truncate">{{ $featured->name }}</h3>
							<p class="learn-by">{{ $project->company_id != NULL ? $project->company->name : '' }}</p>
							<p>{{ $project->description }}</p>
							
							<div class="learn-action">
								<div class="progress">
									<div class="determinate" style="width: 70%"></div>
								</div>
								<div class="truncate s12">
								<ul id="projects-team">
								@foreach($project->team as $member)
									<li><a href="#!"><img class="circle member" src=""></a></li>
								@endforeach
									<li class="count"><a href="#">+2 Open</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


	
@endsection

@section('js')
<script type="text/javascript">
      $(function() {
		  var message = {
		
		    message: [ 
		      'Video Animation',
		      'Web Design',
		      'Kickstarter', 
		      'Graphics',
		      'SEO Optimization',
		      'Social Media',
		    ],
		    counterS: 0,
		    counterL: 0,
		    deleteS: false,
		
		    init: function() {
		      this.cacheElem();
		      this.type();
		    },
		
		    cacheElem: function() {
		      this.$text = $('.text');
		    },
		
		    type: function() {
		      var message   = this.message[this.counterS],
		          that      = this,
		          speed     = 0;
		
		      message = !this.deleteS ? message.slice(0, ++this.counterL) : message.slice(0, --this.counterL);
		      if(this.message[this.counterS] != message && !this.deleteS) {
		        this.$text.text(message);
		        speed = 90;
		      }
		      else {
		        this.deleteS = true;
		        speed = this.message[this.counterS] == message ? 1500 : 40;
		        this.$text.text(message);
		        if (message == '') {
		          this.deleteS = false;
		          this.counterS = this.counterS < this.message.length - 1 ? this.counterS + 1 : 0;
		        }
		      }
		      setTimeout(function(){that.type()}, speed);
		    }
		  };
		  message.init();
		});
</script>
@endsection
