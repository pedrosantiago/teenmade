<!-- Modal -->
<div class="modal fade" id="create-project" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Create Project</h4>
			</div>
			{{ Form::open(['url' => 'projects', 'files' => true]) }}
			<div class="modal-body">
				
				<div class="row">
					<fieldset class="form-group col-md-8">
						<label for="name">Name</label>
						{{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name of your project, please!']) }}
						<small class="text-muted">{{ $errors->first('name') }}</small>
					</fieldset>
					<fieldset class="form-group col-md-4">
						<label for="email">Completed When ?</label>
						{{ Form::text('completed_at', null, ['id' => 'completed_at', 'class' => 'form-control', 'placeholder' => 'mm/dd/yyyy']) }}
						<small class="text-muted">{{ $errors->first('completed_at') }}</small>
					</fieldset>
				</div>
				<fieldset class="form-group">
					<label for="email">Small Description</label>
					{{ Form::textarea('description', null, ['rows' => 4, 'id' => 'completed_at', 'class' => 'form-control', 'placeholder' => 'Size of a tweet please.']) }}
					<small class="text-muted">{{ $errors->first('description') }}</small>
				</fieldset>
				<fieldset class="form-group">
					<label for="email">Photo</label>
					{{ Form::file('photo', ['id' => 'photo', 'class' => 'form-control']) }}
					<small class="text-muted">{{ $errors->first('photo') }}</small>
				</fieldset>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Create Project</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->