<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	
	    <title>{{ $title or 'TeenMade' }}</title>
	
	    <!-- Bootstrap core CSS -->
	    <link href="{{ asset('css/teenmade.css') }}" rel="stylesheet">
		@yield('css')
		
	</head>
	<body>
	
	<header>
		
		<nav id="menu" class="navbar navbar-light navbar-fixed-top bd-navbar {{ (isset($menu) && $menu == 'transform') ? 'alternative nav-transform' : 'default' }}">
			<div class="container">
				<a class="navbar-brand" href="{{ url('') }}">
					<img class="brand-logo default" src="{{ asset('img/tm-logo-red.png') }}" />
					<img class="brand-logo alternative" src="{{ asset('img/tm-logo.png') }}" />
				</a>
				
				<ul class="nav navbar-nav nav-center">
					<li class="nav-item">
						<a class="nav-link {{  Request::is('discover') ? 'active' : '' }}" href="{{ url('discover') }}">Discover</a>
					</li>
					<li class="nav-item">
						<a class="nav-link {{  Request::is('services') || Request::is('services/*') ? 'active' : '' }}" href="{{ url('services') }}">Services</a>
					</li>
					<li class="nav-item">
						<a class="nav-link {{  Request::is('shops') || (isset($shop) && Request::is($shop->slug)) ? 'active' : '' }}" href="{{ url('shops') }}">Shops</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " href="blog.teenmade.com">Learn</a>
					</li>
				</ul>
				
				<ul class="nav navbar-nav pull-right">
					@if(Auth::guest())
					<li class="nav-item">
						<button class="nav-link btn-link" data-toggle="modal" data-target="#login">Login</button>
					</li>
					<li class="nav-item">
						<a class="btn btn-primary-outline btn-round" data-toggle="modal" data-target="#signup">Signup</a>
					</li>
					@else 
					
					
					
					<li class="nav-item">
						<div class="btn-group">
						  <button type="button" style="border:none;" class="btn btn-primary btn-round dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    {{ Auth::user()->name }}
						  </button>
						  <div class="dropdown-menu">
						  
						  @if(Auth::user()->is_elite)
						  	<a class="dropdown-item" href="{{ url('elite') }}"><strong class="text-primary">Elite Panel</strong></a>
						  	<div class="dropdown-divider"></div>
						  @endif
						  
						  	@if(Auth::user()->type == 'member')
						  		<a class="dropdown-item" href="{{ url('me') }}"><strong>My Profile</strong></a>
						  		<a class="dropdown-item" href="{{ url('me/shop') }}"><strong>My Shop</strong></a>
						  		<div class="dropdown-divider"></div>
						  	@endif
						  	
						    <a class="dropdown-item" href="{{ url('order-history') }}">Orders</a>
						    <a class="dropdown-item" href="{{ url('wishlist') }}">Wish List</a>
						    <a class="dropdown-item" href="{{ url('cart') }}">Cart</a>
						    <a class="dropdown-item" href="{{ url('me/edit') }}">Account</a>
						    <div class="dropdown-divider"></div>
						    <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
						  </div>
						</div>
					</li>
					@endif
				</ul>
			</div>
		</nav>
		@hasSection('header')
        	@yield('header')
	    @else
	        <span class="spacer-main"></span>
	    @endif
		
	</header>
	
	@yield('content')
	

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    
    <script src="{{ asset('js/type.min.js') }}"></script>   
    <script src="{{ asset('js/tether.min.js') }}"></script>
    <script src="{{ asset('js/slick.min.js') }}"></script>
    
    <script src="{{ asset('js/notifications.js') }}"></script>
    
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/teenmade.js') }}"></script>
    
    
    @yield('js')
    
    <script type="text/javascript">
	    $(document).ready(function(){
		    @if(Session::has('modal'))
		    	$('#<?php echo Session::get('modal'); ?>').modal('show');
		    @endif
		    
		    @if(Session::has('prompt'))
		    	$('#message').modal('show');
		    @endif
		    
		    @if(Session::has('notification'))
			<?php
				$notification = Session::get('notification');
				
				if(is_array($notification)){
				$icon = array_key_exists('icon', $notification) ? '<i class="mdi mdi-'.$notification['icon'].'"></i>' : '<i class="mdi mdi-check"></i>' ;
				
				$text = $notification['message'];
				
				} else {
					$icon = '<i class="mdi mdi-check"></i>';
					$text = $notification;
				}
				
			?>
		// create the notification
		new NotificationFx({
			message : '<?php echo $icon.'<p>'.$text.'</p>' ; ?>',
			layout : 'bar',
			effect : 'slidetop',
			type : 'notice',
		}).show();

@endif
		    
	    });
		
	</script>
	
	
    
    @if(Session::has('view'))
    	@include(Session::get('view'))
    @endif
		
	@if(Auth::guest())
		@include('user.login-modal')
		@include('user.create-modal')
		@include('user.forgot-modal')
	@endif
	
	@if(Session::has('prompt'))
	<div class="modal fade" tabindex="-1" id="message" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">{{ Session::get('prompt.title') }}</h4>
      </div>
      	<div class="modal-body">
      		{{ Session::get('prompt.content') }}
      	</div>
      	<div class="modal-footer">
	    	<button type="submit" class="btn btn-primary" data-dismiss="modal">Ok</button>
	    </div>
	    </div>
	  </div>
	</div>
	@endif


  </body>
</html>

