@extends('_email.layout')

@section('content')
	Click here to reset your password: <a href="{{ $link = url('recover-password', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>
@endsection