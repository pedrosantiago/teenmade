@extends('_email.layout')

@section('content')
	Hi There, how are you doing? <br /><br />

	We may have one more TeenMade <strong>{{ $service->title }}</strong> project! That's awesome! <br />
	Here below are the details of our future client. Please contact him soon! <br /><br />
	
	<hr />
	
	<strong>Name:</strong> {{ $data->name }} <br />
	<strong>Email:</strong> {{ $data->email }} <br />
	<strong>Company:</strong> {{ $data->company }} <br />
	<strong>Phone:</strong> {{ $data->phone }} <br />
	And last, but not least, a short description of our client's plans: <br><br/>
	
	"<i>{{ nl2br($data->description) }}</i>"
	
	Thanks, please answer (not me, the client).
		
@endsection