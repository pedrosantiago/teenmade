@extends('_email.layout')

@section('content')
	<h2>Hi There {{ $user->name }},</h2>
	
	<p>Unfortunatelly, your product <strong>{{ $product->name }}</strong> will not be available in TeenMade for now.</p>
	<p>Our team does this moderation in order to keep the quality at the most top level and assure that everything is fine always.</p>
	
	<hr />
	
	@if($reason != '')
		<p>Here is the feedback that an Elite TeenMade Member has made:</p>
		<i>"{{ $reason }}"</i>
	@endif
	
	<br><br>
	We hope that this somehow help you.<br />
	TeenMade Team
@endsection