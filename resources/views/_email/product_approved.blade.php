@extends('_email.layout')

@section('content')
	<h2>Hi There {{ $user->name }},</h2>
	
	<p>Wohooooooooo!! Your Product {{ $product->name }} has been approved and it's now available on TeenMade website!</p>
	

	<br><br>
	We wish you great sales!<br />
	TeenMade Team
@endsection