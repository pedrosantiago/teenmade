@extends('layout')

@section('content')
<div class="container">
	<h1>My Store</h1>
	
	@include('shop._menu')
	
	@if($orders->count() == 0)
		<div class="jumbotron">
			<h2 class="display-4">Oh no =/ </h2>
			<h3>You don't have any orders yet. No worries, it will happen sometime! </h3>
		</div>	
	@else
	
	@foreach($orders as $order)
	<div class="card m-b-3 order">
		<div class="card-header">
			<div class="row">
				<div class="col-md-3">
					<label>Order Placed</label>
					{{ Carbon::parse($order->created_at)->format('d F Y')}}
				</div>
				<div class="col-md-2">
					<label>Total</label>
					{{ '$'.money($order->total) }}
				</div>
				<div class="col-md-4">
					<label>Dispach To</label>
					{{ $order->address->name }}
				</div>
				<div class="col-md-3 text-xs-right">
					<label >Order</label>
					#{{$order->order_id}}
				</div>
			</div>
		</div>
		<div class="card-block">
		
		<div class="row">
			<div class="col-md-8">
				<h4>Items</h4>
				
				<ul class="list-group">
				@foreach($order->products as $product)
					<li class="list-group-item">{{ $product->name }} 
					
					@if($product->pivot->shipped_at == null)
						<a href="{{ url('me/shop/orders/'.$order->order_id.'/'.$product->product_id) }}" class="pull-right btn btn-primary btn-sm" style="margin-top:-4px;">Mark as Sent</a>
					@else
						<span class="text-success pull-right"><i class="mdi mdi-check"></i>Sent!</span>
					@endif
					</li>
  
				@endforeach
				</ul>
			</div>
			<div class="col-md-4">
				<h4>Address</h4>
				<blockquote class="blockquote" style="font-size:1em;border-width:1px;">
				<address>			
					<strong>{{ $order->address->company }}</strong><br>
					{{ $order->address->address }}<br>
					{{ $order->address->city }}, {{ $order->address->province }} {{ $order->address->zip_code }}<br>
					@if($order->address->phone != '') 
						<abbr title="Phone">P:</abbr> {{ $order->address->phone }}
					@endif
				</address>
				</blockquote>
				
			</div>
		</div>

	</div>
	@endforeach
	
	@endif
</div>
@endsection