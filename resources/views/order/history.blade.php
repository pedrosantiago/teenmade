@extends('layout')

@section('content')

<div class="container">
	<h1>Order History</h1>
	
	@foreach($orders as $order)
	<div class="card m-b-3 order">
		<div class="card-header">
			<div class="row">
				<div class="col-md-3">
					<label>Order Placed</label>
					{{ Carbon::parse($order->created_at)->format('d F Y')}}
				</div>
				<div class="col-md-2">
					<label>Total</label>
					{{ '$'.money($order->total) }}
				</div>
				<div class="col-md-4">
					<label>Dispach To</label>
					{{ $order->address->name }}
				</div>
				<div class="col-md-3 text-xs-right">
					<label >Order</label>
					#{{$order->order_id}}
				</div>
			</div>
		</div>
		<div class="card-block">
			@foreach($order->products as $product)
				<div class="row">
					<div class="col-md-2">
						<img src="{{ asset('img/medium/'.$product->photos->first()->photo) }}" class="img-thumbnail" />
					</div>
					<div class="col-md-7">
						<h4 style="font-weight:400;padding-top:5px;" class="m-a-0">{{ $product->quantity > 1 ? $product->quantity.'x ' : '' }}<a href="#">{{ $product->name }}</a></h4>
						<div class="product-desc">Sold By {{ $product->user->name }} ({{ $product->shop->name }})</div>
						
						<hr>
						<h4 class="product-price m-a-0">${{ money($product->price) }}</h4>
						<h5 class="product-stock m-a-0 text-success" style="padding-top:5px;">{{ $product->pivot->shipped_at != null ? 'Delivered!' : 'On its way.' }}</h5>


					</div>
					<div class="col-md-3" style="padding-top:5px;">
						{{ Form::open(['url' => 'cart'])}}
							{{ Form::hidden('product_id', $product->product_id) }}
							{{ Form::hidden('quantity', 1) }}
							<button type="submit" class="btn btn-block btn-secondary" style="margin-bottom:7px;">Buy it Again</button>
						{{ Form::close() }}
						<a href="{{ url($product->shop->slug) }}" class="btn btn-block btn-secondary">Visit Seller Store</a>
						<button class="btn btn-block btn-link" data-toggle="modal" data-product_id="{{ $product->product_id }}" data-order_id="{{  $order->order_id }}" data-target="#contact-seller">Contact Seller</button>
					</div>
				</div>
			@endforeach
		</div>

	</div>
	@endforeach
	
</div>

<!-- Modal -->
<div class="modal fade" id="contact-seller" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Contact Seller</h4>
			</div>
			{{ Form::open(['url' => 'message'])}}
				<input type="hidden" name="product_id" value="" id="product_id" />
				<input type="hidden" name="order_id" value="" id="order_id" />
				
			<div class="modal-body">
			
				<div class="row">
					<fieldset class="form-group col-md-4">
						<label for="name">Name</label>
						{{ Form::text('name', \Auth::check() ? \Auth::user()->name : null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Your name, please.']) }}
						<small class="text-muted">{{ $errors->first('name') }}</small>
					</fieldset>
					<fieldset class="form-group col-md-8">
						<label for="email">Email</label>
						{{ Form::text('email', \Auth::check() ? \Auth::user()->email : null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'What\'s your email?']) }}
						<small class="text-muted">{{ $errors->first('email') }}</small>
					</fieldset>
				</div>
				<fieldset class="form-group">
						<label for="email">Message</label>
						{{ Form::textarea('message', null, ['id' => 'email', 'class' => 'form-control', 'rows' => '5', 'placeholder'=> 'Hey there....']) }}
						<small class="text-muted">{{ $errors->first('message') }}</small>
					</fieldset>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-primary">Send Message</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function(){
		$('#contact-seller').on('show.bs.modal', function (event) {
		  var button = $(event.relatedTarget); // Button that triggered the modal
		  $('#order_id').val(button.data('order_id')); // Extract info from data-* attributes
		  $('#product_id').val(button.data('product_id'));
		  
		});
	})
</script>
@endsection
