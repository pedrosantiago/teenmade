@extends('layout')

@section('content')

	<div class="container">
		<h1>Checkout</h1>
		
		{{ Form::open(['url' => 'checkout', 'id' => 'place_order']) }}
		<div class="row">
			<div class="col-md-9">
				<h2>Address <button class="btn btn-primary btn-sm pull-right" type="button" data-toggle="modal" data-target="#create-address">Create Address</button></h2>


				<div class="row">
				
					@foreach($user->addresses as $address)
						<div class="col-md-6">
							<label for="address{{ $address->address_id }}" class="c-input c-radio">
								<div class="card card-hover address">
									<div class="card-header"><span class="c-indicator"></span> 
									{{ Form::radio('address_id', $address->address_id, false, ['id' => 'address'.$address->address_id]) }}
									{{ $address->name }}</div>
									<div class="card-block">
										<address>			
											<strong>{{ $address->company }}</strong><br>
											{{ $address->address }}<br>
											{{ $address->city }}, {{ $address->province }} {{ $address->zip_code }}<br>
											@if($address->phone != '') 
												<abbr title="Phone">P:</abbr> {{ $address->phone }}
											@endif
										</address>
									</div>
								</div>
							</label>
						</div>
					@endforeach
					
				</div>
				<small class="text-muted">{{ $errors->first('address_id') }}</small>
				<br><br>
				
				<h2>Payment Method</h2>
				
				<div class="row">
					<div class="col-md-6">
						<fieldset class="form-group">
							<label for="name">Card Number</label>
							{{ Form::text('card_number', null, ['id' => 'card_number', 'class' => 'form-control', 'data-stripe' => 'number']) }}
							<small class="text-muted">{{ $errors->first('card_number') }}</small>
						</fieldset>
						<fieldset class="form-group">
							<label for="name">Full Name</label>
							{{ Form::text('card_name', null, ['id' => 'card_name', 'class' => 'form-control']) }}
							<small class="text-muted">{{ $errors->first('card_name') }}</small>
						</fieldset>
						<div class="row">
							<fieldset class="form-group col-md-6">
								<label for="name">Valid Thru</label>
								
																<div class="input-group">
     
      {{ Form::text('card_month', null, ['id' => 'card_month', 'class' => 'form-control', 'data-stripe' => 'exp_month', 'placeholder' => 'MM']) }}
	  <span class="input-group-addon">/</span>
      {{ Form::text('card_year', null, ['id' => 'card_year', 'class' => 'form-control', 'data-stripe' => 'exp_year', 'placeholder' => 'YYYY']) }}

    </div>

								<small class="text-muted">{{ $errors->first('card_expiry') }}</small>
							</fieldset>
							<fieldset class="form-group col-md-6">
								<label for="name">CVC</label>
								{{ Form::text('card_cvc', null, ['id' => 'card_cvc', 'class' => 'form-control', 'data-stripe' => 'cvc']) }}
								<small class="text-muted">{{ $errors->first('card_cvc') }}</small>
							</fieldset>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card-wrapper" style="margin-top:30px;"></div>
					</div>
				</div>
				<br><br>
				

				
				<h2>Review Items</h2>		
				@include('cart._table')
				
				<br />
				<button class="btn btn-primary btn-lg submit">Place Order</button>	
			</div>
			<div class="col-md-3" id="summary">
				<div class="card card-secondary">
					
					<div class="card-block">
						<h3 class="card-title">Order Summary</h3>
						<dl class="dl-horizontal row">
						  <dt class="col-sm-6">Items:</dt>
						  <dd class="col-sm-6 text-xs-right subtotal">{{ '$'.money(Cart::getSubTotal()) }}</dd>
						
						  <dt class="col-sm-6">Shipping:</dt>
						  <dd class="col-sm-6 text-xs-right shipping"><?php $shipping = array_sum(array_pluck(Cart::getContent()->toArray(), 'attributes.shipping')); echo '$'.money($shipping); ?></dd>
						  
						 
						  
						</dl>
						 <hr />
						  
						  <h5>Total: <span class="total">{{ '$'.money(Cart::getSubTotal() +  $shipping) }}</span></h5>
						  
						  <button class="submit btn btn-block btn-primary btn-lg">Place Order</button>	
					</div>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
	
	<div class="modal fade" id="stripe_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Payment Error</h4>
			</div>
			<div class="modal-body" id="payment-errors">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Okay</button>

			</div>
		</div>
		</div>
	</div>
	@include('address.create-modal')
	
	@section('js')
		<!-- Stripe JS -->
		<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
		<script type="text/javascript" src="{{ asset('js/jquery.card.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery.touchspin.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/sticky.min.js') }}"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			Stripe.setPublishableKey('<?php echo Config::get('services.stripe.publishable_key'); ?>');
	        $('form').card({
			    container: '.card-wrapper', // *required*
			    formSelectors: {
			        numberInput: 'input#card_number', // optional — default input[name="number"]
			        expiryInput: 'input#card_month, input#card_year', // optional — default input[name="expiry"]
			        cvcInput: 'input#card_cvc', // optional — default input[name="cvc"]
			        nameInput: 'input#card_name' // optional - defaults input[name="name"]
			    }
			});
			
			var $form = $('#place_order');
			$form.submit(function(event) {
				// Disable the submit button to prevent repeated clicks:
				$form.find('.submit').prop('disabled', true);
				
				// Request a token from Stripe:
				Stripe.card.createToken($form, stripeResponseHandler);
				
				// Prevent the form from being submitted:
				return false;
			});
			
			function stripeResponseHandler(status, response) {
				// Grab the form:
				var $form = $('#place_order');
				
				if (response.error) { // Problem!
					
					// Show the errors on the form:
					$('#payment-errors').text(response.error.message);
					$form.find('.submit').prop('disabled', false); // Re-enable submission
					
					$('#stripe_info').modal('show');
				
				} else { // Token was created!
					
					// Get the token ID:
					var token = response.id;
					
					// Insert the token ID into the form so it gets submitted to the server:
					$form.append($('<input type="hidden" name="stripe_token">').val(token));
					
					// Submit the form:
					$form.get(0).submit();
				}
			};
						
			$('#address<?php echo old('address_id');?>').parent().parent().addClass('card-active');
			
			$('.address').click(function(){
				$('.address').removeClass('card-active');
				$(this).addClass('card-active');
			});
			
			$("input.quantity").TouchSpin().on("touchspin.on.stopspin", function(e) {
				id = $(this).data('id');
				$.post('<?php echo url('cart');?>/'+id, { quantity: $(this).val(), _token: '<?php echo csrf_token();?>', }, function(res){
					new NotificationFx({
						message : '<i class="mdi mdi-check"></i><p>'+res.message+'</p>',
						layout : 'bar',
						effect : 'slidetop',
						type : 'notice',
						ttl: 1000
					}).show();
					
					$('.subtotalPrice'+id).html(res.price);
					$('.subtotal').html(res.subtotal);
					$('.shipping').html(res.shipping);		
					$('.total').html(res.total);	
				});
				
			});
			
			$("#summary").stick_in_parent({
				offset_top: 100
			});
			
			
		});
    	</script>
	@endsection

@endsection