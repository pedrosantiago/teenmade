<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Login</h4>
      </div>
	  {{ Form::open(['url' => 'login', 'class' => 'login-form']) }}
		<div class="modal-body">
			<fieldset class="form-group">
				<label for="email">Email</label>
				{{ Form::text('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'E.g. arya.stark@winterfell.com']) }}
				<small class="text-muted">{{ $errors->first('email') }}</small>
			</fieldset>
			<fieldset class="form-group">
				<label for="password">Password</label>
				{{ Form::password('password', ['id' => 'password', 'class' => 'form-control', 'placeholder' => 'E.g. needle']) }}
				<small class="text-muted">{{ $errors->first('password') }}</small>
			</fieldset>
		</div>
	    <div class="modal-footer">
	    	<a class="btn btn-link" data-dismiss="modal" data-toggle="modal" href="#forgot">Forgot My Password</a>
	    	<a class="btn btn-secondary" data-dismiss="modal" data-toggle="modal" href="#signup">Create Account</a>
	    	<button type="submit" class="btn btn-primary">Login</button>
	    </div>
	    {{ Form::close() }}
      </div>

    </div>
  </div>
</div>