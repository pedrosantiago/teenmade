<div class="row">
	<fieldset class="form-group col-md-4">
		<label for="username">Username</label>
		{{ Form::text('username', null, ['id' => 'username', 'class' => 'form-control', 'placeholder' => 'That will be your identity on teenmade.']) }}
		<small class="text-muted">{{ $errors->first('username') }}</small>
	</fieldset>
	<fieldset class="form-group col-md-4">
		<label for="avatar">Avatar Picture</label>
		{{ Form::file('avatar', ['id' => 'avatar', 'class' => 'form-control']) }}
		<small class="text-muted">{{ $errors->first('avatar') }}</small>
	</fieldset>
	<fieldset class="form-group col-md-4">
		<label for="cover_picture">Cover Photo</label>
		{{ Form::file('cover_picture', ['id' => 'cover_picture', 'class' => 'form-control']) }}
		<small class="text-muted">{{ $errors->first('cover_picture') }}</small>
	</fieldset>
</div>
<fieldset class="form-group">
	<label for="small_bio">Small Bio</label>
	{{ Form::textarea('small_bio', null, ['id' => 'small_bio', 'class' => 'form-control', 'placeholder' => 'Just talk about yourself...', 'rows' => 3]) }}
	<small class="text-muted">{{ $errors->first('small_bio') }}</small>
</fieldset>