<div class="modal fade" id="forgot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Forgot Password</h4>
      </div>
	  {{ Form::open(['url' => 'forgot-password', 'class' => 'login-form']) }}
		<div class="modal-body">
			<p>Hey, don't worry. Sometimes it happens. Please, type in your e-mail. If it's registered we'll send you a link. With that will be possible to define a new password for you. Okay?</p>
			<fieldset class="form-group">
				<label for="email">Email</label>
				{{ Form::text('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Enter e-mail']) }}
				<small class="text-muted">{{ $errors->first('email') }}</small>
			</fieldset>
		</div>
	    <div class="modal-footer">
	    	<button type="submit" class="btn btn-primary">Recover My Password</button>
	    </div>
	    {{ Form::close() }}
      </div>

    </div>
  </div>
</div>