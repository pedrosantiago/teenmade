<div class="modal fade" id="recover" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Recover Password</h4>
      </div>
	  {{ Form::open(['url' => 'update-password', 'class' => 'login-form']) }}
	  	{{ Form::hidden('token', Session::pull('token')) }}
		<div class="modal-body">
			<fieldset class="form-group">
				<label for="email">Email</label>
				{{ Form::text('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Enter e-mail']) }}
				<small class="text-muted">{{ $errors->first('email') }}</small>
			</fieldset>
			<fieldset class="form-group">
				<label for="password">New Password</label>
				{{ Form::password('password', ['id' => 'password', 'class' => 'form-control']) }}
				<small class="text-muted">{{ $errors->first('password') }}</small>
			</fieldset>
			<fieldset class="form-group">
				<label for="password">Confirm Password</label>
				{{ Form::password('password_confirmation', ['id' => 'password_confirmation', 'class' => 'form-control']) }}
				<small class="text-muted">{{ $errors->first('password_confirmation') }}</small>
			</fieldset>
		</div>
	    <div class="modal-footer">
	    	<button type="submit" class="btn btn-primary">Recover My Password</button>
	    </div>
	    {{ Form::close() }}
      </div>

    </div>
  </div>
</div>