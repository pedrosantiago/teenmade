@extends('layout')


@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="card m-b-3" style="overflow:hidden;">
		 		<img class="card-img-top" alt="100%x180" style="border-bottom:1px solid #ccc;margin-top:-1px;margin-left:-1px;" src="{{ asset('img/cover/'.$user->cover_picture) }}">
				<div class="card-block text-xs-center">
					<img src="{{ asset('img/avatar/'.$user->avatar) }}" class="avatar" />
					<h4 class="card-title text-xs-center">{{ $user->name }}</h4>
					<p class="card-text">{{ $user->small_bio }}</p>
				</div>
				@if($user->user_id == Auth::id())
				<div class="card-footer">
					
						<a href="{{ url('me/edit') }}" class="btn btn-secondary">Edit Profile</a>
					
				</div>
				@endif
			</div>
			
			<div class="card">
				<div class="card-block">
					<h3 class="card-title">Skills</h3>
					<p class="card-text">
					
						@foreach($user->skills as $skill)
						

						
						<span style="font-size:14px;padding:3px 10px;" class="label label-pill label-default">{{ $skill->name }}</span>

						@endforeach
					</p>
				</div>
				@if($user->user_id == Auth::id())
				<div class="card-footer">
					{{ Form::open(['url' => 'me/skill']) }}
					<div class="input-group">
				      <input type="text" class="form-control" name="skill" id="skill" placeholder="Type in your skill...">
				      <span class="input-group-btn">
				        <button class="btn btn-secondary" type="submit">Add</button>
				      </span>
				    </div>
					{{ Form::close() }}
				</div>
				@endif
			</div>
		</div>
		
		<div class="col-md-8">
			<h2>Shop</h2>
			@if($user->shop != null)
				@include('shop._card', ['show_footer' => true])
			@else
			<div class="jumbotron p-a-2">
				<p class="lead m-a-0 m-b-2">Create your shop and start selling products right away! Create your shop and start selling products right away! Create your shop and start selling products right away! Create your shop and start selling products right away!</p>	
				<a href="{{ url('shop/create') }}" class="btn btn-primary">Create My Shop</a>
			</div>
			@endif
		
		
			<h2 class="m-t-3">Projects</h2>
			<div class="row">
				@foreach($user->projects as $project)
				<div class="col col-md-6">
					<div class="card card-inverse">
					  <img class="card-img img-fluid"  src="{{ asset('img/large/'.$project->photo) }}" alt="Card image">
					  <div class="card-img-overlay" style="background-color:rgba(0,0,0,.3)">
					    <h4 class="card-title">{{ $project->name }}</h4>
					    <p class="card-text" style="color:white;">{{ $project->description }}</p>
					    <p class="card-text"><small class="text-muted" style="color:#fff">Last updated 3 mins ago</small></p>
					    
					    @if(Auth::check() && Auth::id() == $project->user_id)
					    {{ Form::open(['url' => 'projects/'.$project->project_id,'method' => 'delete'])}}
					    	<button type="submit" class="btn btn-secondary-outline btn-sm btn-round" style="position:absolute;bottom:20px;"><i class="mdi mdi-delete"></i></button>
					    {{ Form::close() }}
					    @endif
					  </div>

					  
					</div>
					
				</div>
				@endforeach
				@if(\Auth::check() && \Auth::id() == $user->user_id)
					<div class="col col-md-6">
						<a href="#create-project" data-toggle="modal" style="text-decoration:none;" class="placeholder"><div class="jumbotron  text-xs-center " style="height400px;padding:132px 0 100px 0;" ><i style="" class="mdi mdi-plus-circle"></i>
							<h4 class="p-t-1" >Create Project</h4>
						</div></a>
					</div>
					@include('project.create-modal')
				@endif
			</div>
		</div>
		
	</div>
</div>

@endsection