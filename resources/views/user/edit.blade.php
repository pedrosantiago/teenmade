@extends('layout')

@section('content')

<div class="row" style="margin-top:140px;">
	<div class="container">
	{{ Form::model($user, ['url' => 'me', 'method' => 'put', 'files' => true]) }}
        
        <h1>My Account</h1>
        
        <h3>Basic Information</h3>
        <div class="row">
			<fieldset class="form-group col-md-4">
				<label for="name">Name</label>
				{{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Your name, please.']) }}
				<small class="text-muted">{{ $errors->first('name') }}</small>
			</fieldset>
			<fieldset class="form-group col-md-8">
				<label for="email">Email</label>
				{{ Form::text('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Enter e-mail']) }}
				<small class="text-muted">{{ $errors->first('email') }}</small>
			</fieldset>
		</div>
		<div class="row">
		
			<fieldset class="form-group col-md-4">
				<label for="password">New Password</label>
				{{ Form::password('password', ['id' => 'password', 'class' => 'form-control']) }}
				<small class="text-muted">{{ $errors->first('password') }}</small>
			</fieldset>
			
			<fieldset class="form-group col-md-4">
				<label for="confirm_password">Confirm Password</label>
				{{ Form::password('confirm_password', ['id' => 'confirm_password', 'class' => 'form-control']) }}
				<small class="text-muted">{{ $errors->first('confirm_password') }}</small>
			</fieldset>
			<fieldset class="form-group col-md-4">
				<label for="age">Age</label>
				{{ Form::select('age', array('' => 'Age', '13', '14', '15', '16', '17', '18', '19', 'adult' => '20+'), null,['id' => 'age', 'class' => 'c-select form-control']) }}
				<small class="text-muted">{{ $errors->first('age') }}</small>
			</fieldset>
		</div>
		<div class="row">
			
		</div>

        
        
        
        
        
        
        
        <br />
        @if($user->type == 'member' && $user->age != '20+')
       	 	<h3>Profile</h3>
        	@include('user._form-profile')	
			<br><br>
        @endif
			<button class="btn btn-primary btn-lg" type="submit" name="action">Save</button>
        
    {{ Form::close() }}
	</div>
</div>
@endsection