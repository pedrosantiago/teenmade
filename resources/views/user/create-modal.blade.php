<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Signup</h4>
      </div>
	  {{ Form::open(['url' => 'signup']) }}
		<div class="modal-body">
			<div class="row">
				<fieldset class="form-group col-md-4">
					<label for="name">Name</label>
					{{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Your name, please.']) }}
					<small class="text-muted">{{ $errors->first('name') }}</small>
				</fieldset>
				<fieldset class="form-group col-md-8">
					<label for="email">Email</label>
					{{ Form::text('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Enter e-mail']) }}
					<small class="text-muted">{{ $errors->first('email') }}</small>
				</fieldset>
			</div>
			<div class="row">
			
				<fieldset class="form-group col-md-6">
					<label for="password">Password</label>
					{{ Form::password('password', ['id' => 'password', 'class' => 'form-control']) }}
					<small class="text-muted">{{ $errors->first('password') }}</small>
				</fieldset>
				
				<fieldset class="form-group col-md-6">
					<label for="confirm_password">Confirm Password</label>
					{{ Form::password('confirm_password', ['id' => 'confirm_password', 'class' => 'form-control']) }}
					<small class="text-muted">{{ $errors->first('confirm_password') }}</small>
				</fieldset>
				
				
				
			</div>
			<div class="row">
				<fieldset class="form-group col-md-4">
					<label for="age">Age</label>
					{{ Form::select('age', array('' => 'Age', '13', '14', '15', '16', '17', '18', '19', 'adult' => '20+'), 'adult' ,['id' => 'age', 'class' => 'c-select form-control']) }}
					<small class="text-muted">{{ $errors->first('age') }}</small>
				</fieldset>
			</div>
				
		</div>
	    <div class="modal-footer">
	    	<button type="submit" class="btn btn-primary">Signup</button>
	    </div>
	    {{ Form::close() }}
      </div>

    </div>
  </div>
</div>