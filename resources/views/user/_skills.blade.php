<table class="table table-bordered m-b-0">
	<thead>
		<tr>
			<th>Skill</th>
			<th>Level</th>
			<th></th>
		</tr>
	</thead>
	@foreach($skills as $skill)
	<tr>
		<th>{{ $skill->name }}</th>
		<td style="margin-top:0;padding-top:9px;">	
			<div class="btn-group" style="margin:0;" data-toggle="buttons">
				@foreach(Config::get('app.skill_level') as $level)
				  <label class="btn btn-black-outline <?php if($level == $skill->pivot->level) echo 'active'; ?> btn-sm m-b-0">
				    	{{ Form::radio("skill[".$skill->skill_id."]", $level, $level == $skill->pivot->level) }}
				    	{{ ucfirst($level) }}  
				   </label>
			  	@endforeach
			</div>
		</td>
		<td class="actions">
			<a href="{{ url('elite/users/'.$user_id.'/skills/'.$skill->skill_id) }}" class="btn btn-secondary btn-sm"><i class="mdi mdi-close"></i></a>
		</td>
	</tr>
	@endforeach
</table>