@extends('layout')

@section('content')
	<div class="container">
		<h1>Hey there {{ Auth::user()->name }},</h1>
		<p class="lead">We've noticed that you are qualified to be part of TeenMade. (Sterling change this accordingly). Please, fill in the rest of the fields and be part of our community.</p>
		<hr /><br><br>
		{{ Form::open(['url' => 'me/teen', 'files' => true])}}
			
			@include('user._form-profile')
			
			<br><br>
			
			<button class="btn btn-primary btn-lg">Save and Proceed</button>
			<a href="{{ url('/') }}" class="btn btn-link">I don't wanna be part of this.</a>
		{{ Form::close() }}
		
	</div>
@endsection