<div class="modal " id="create-address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Create Address</h4>
      </div>
	  {{ Form::open(['url' => 'address']) }}
		<div class="modal-body">
			<div class="row">
			<fieldset class="form-group col-md-8">
					<label for="name">Full Name</label>
					{{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Your name, please.']) }}
					<small class="text-muted">{{ $errors->first('name') }}</small>
			</fieldset>
			<fieldset class="form-group col-md-4">
					<label for="company">Company</label>
					{{ Form::text('company', null, ['id' => 'company', 'class' => 'form-control', 'placeholder' => 'Optional']) }}
					<small class="text-muted">{{ $errors->first('company') }}</small>
			</fieldset>
			</div>
			<fieldset class="form-group">
				<label for="address">Address</label>
				{{ Form::text('address', null, ['id' => 'address', 'class' => 'form-control', 'placeholder' => 'E.g: Ocean Avenue, 800']) }}
				<small class="text-muted">{{ $errors->first('address') }}</small>
			</fieldset>
			<div class="row">		
				<fieldset class="form-group col-md-5">
					<label for="password">City</label>
					{{ Form::text('city', null, ['id' => 'city', 'class' => 'form-control']) }}
					<small class="text-muted">{{ $errors->first('city') }}</small>
				</fieldset>
				
				<fieldset class="form-group col-md-4">
					<label for="province">State/Province</label>
					{{ Form::text('province', null, ['id' => 'province', 'class' => 'form-control']) }}
					<small class="text-muted">{{ $errors->first('province') }}</small>
				</fieldset>
				
				<fieldset class="form-group col-md-3">
					<label for="zip_code">Zip Code</label>
					{{ Form::text('zip_code', null, ['id' => 'zip_code', 'class' => 'form-control']) }}
					<small class="text-muted">{{ $errors->first('zip_code') }}</small>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="form-group col-md-5">
					<label for="zip_code">Phone</label>
					{{ Form::text('phone', null, ['id' => 'phone', 'class' => 'form-control']) }}
					<small class="text-muted">{{ $errors->first('phone') }}</small>
				</fieldset>
			</div>
		</div>
	    <div class="modal-footer">
	    	<button type="submit" class="btn btn-primary">Create Address</button>
	    </div>
	    {{ Form::close() }}
      </div>

    </div>
  </div>
</div>