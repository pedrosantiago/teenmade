@extends('layout')

@section('content')
<div class="container">
	<h1>My Store</h1>
	
	@include('shop._menu')
	
	@if($products->count() == 0)
		<div class="jumbotron">
			<h2 class="display-4">Oh no =/ </h2>
			<h3>Seems like you have not created your first product yet!. It's simple, easy and amazingly fast. </h3>
			<a class="btn btn-primary btn-lg " href="{{ url('me/shop/products/create') }}">Get Started</a>
		</div>
	@else
		<a class="btn btn-primary-outline pull-right m-b-2" href="{{ url('me/shop/products/create') }}">Create Product</a>
		
		@include('product._table')
		
	@endif
</div>
@endsection