	<div class="row">
		<div class="col-md-7">
			<h2>Basic Information</h2>
			<div class="row">
				<fieldset class="form-group col-md-8">
					<label for="name">Name</label>
					{{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control']) }}
					<small class="text-muted">{{ $errors->first('name') }}</small>
				</fieldset>
				
				<fieldset class="form-group col-md-4">
					<label for="slug">Custom URL</label>
					{{ Form::text('slug', null, ['id' => 'slug', 'class' => 'form-control']) }}
					<small class="text-muted">{{ $errors->first('slug') }}</small>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="form-group col-md-4">
					<label for="name">Category</label>
					{{ Form::select('category_id', $categories, null, ['id' => 'category_id', 'class' => 'form-control c-select']) }}
					<small class="text-muted">{{ $errors->first('category_id') }}</small>
				</fieldset>
				<fieldset class="form-group col-md-3">
					<label for="name">Price</label>
					
					<div class="input-group">
				      <div class="input-group-addon">$</div>
				      {{ Form::text('price', isset($product) ? money($product->price) : null, ['id' => 'price', 'class' => 'form-control money', 'placeholder' => '0.00']) }}
				    </div>
					<small class="text-muted">{{ $errors->first('price') }}</small>
				</fieldset>
				<fieldset class="form-group col-md-3">
					<label for="name">Shipping</label>
					<div class="input-group">
				    	<div class="input-group-addon">$</div>
						{{ Form::text('shipping', isset($product) ? money($product->shipping) : null, ['id' => 'shipping', 'class' => 'money form-control', 'placeholder' => '0.00']) }}
					</div>
					<small class="text-muted">{{ $errors->first('shipping') }}</small>
				</fieldset>
				
				<fieldset class="form-group col-md-2">
					<label for="name">Inventory</label>
					{{ Form::text('inventory', null, ['id' => 'inventory', 'class' => 'form-control']) }}
					<small class="text-muted">{{ $errors->first('inventory') }}</small>
				</fieldset>
			</div>

			<fieldset class="form-group">
				<label for="name">Description</label>
				{{ Form::textarea('description', null, ['id' => 'description', 'class' => 'form-control' , 'rows' => 4]) }}
				<small class="text-muted">{{ $errors->first('description') }}</small>
			</fieldset>
		</div>
		<div class="col-md-5">
			<h2>Photos</h2>
			<div class="jumbotron p-a-2" style="min-height:314px;">
				<div class="row fileupload-buttonbar">
		            <div class="col-lg-3">
		                <!-- The fileinput-button span is used to style the file input field as button -->
		                <span class="btn btn-secondary fileinput-button">
		                    <i class="glyphicon glyphicon-plus"></i>
		                    <span>Add Photos...</span>
		                    <input type="file" name="files[]" multiple>
		                </span>
		                <!-- The global file processing state -->
		                <span class="fileupload-process"></span>
		            </div>
				</div>
				<hr /><br>
    	
	        	<div class="files row">
	        		<?php
		        		if(isset($product))
		        			$photos = array_column($product->photos->toArray(), 'photo', 'photo_id');
		        			
		        		if(old('photos'))
		        			$photos = old('photos');
		        			
		        		if(isset($photos)):
		        			foreach($photos as $key=>$photo):
					?>
	    				<div class="col-md-4 template-download m-b-2">
					        <span class="preview">
					            <a href="{{ url('img/small', $photo) }}" download="{{ url('img/large', $photo) }}">
					            	
					            	<button class="btn btn-secondary btn-sm delete btn-round pull-right" style="padding:5px;margin-top:-10px;margin-left:-10px;position:absolute" data-type="DELETE" {{ isset($product) ? 'data-data=photo_id='.$key.'' : '' }} data-url="{{ url('me/shop/products/photo', $photo) }}"><i class="mdi mdi-delete"></i></button>
					            	<img src="{{ url('img/small', $photo) }}" style="width:120px;height:120px;" class="img-thumbnail img-fluid">
					            	<input type="hidden" name="photos[]" value="{{ $photo }}" />
					            </a>
					        </span>
					    </div>
					<?php endforeach; endif;?>
	        	</div>
			</div>
		</div>
	</div>