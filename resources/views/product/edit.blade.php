@extends('layout')

@section('content')
<div class="container">
	<h1>Edit Product</h1>
	
	
	@include('shop._menu')

	{{ Form::model($product, ['url' => '/me/shop/products/'.$product->product_id , 'files' => true, 'method' => 'put','id' => 'fileupload']) }}
		@include('product._form')
		<button class="btn btn-lg btn-primary" id="submitBtn" type="submit" name="action">Save</button>
	{{ Form::close() }}
	
	
</div>
@endsection

@section('js')

<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <div class="col-md-4 template-download fade m-b-2">
        <span class="preview">
            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery>
            	
            	<button class="btn btn-secondary btn-sm delete btn-round pull-right" style="padding:5px;margin-top:-10px;margin-left:-10px;position:absolute" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}><i class="mdi mdi-delete"></i></button>
            	<img src="{%=file.thumbnailUrl%}" style="width:120px;height:120px;" class="img-thumbnail img-fluid">
            	<input type="hidden" name="photos[]" value="{%=file.hash%}" />
            </a>
        </span>
    </div>
{% } %}
</script>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}

    	<div class="template-upload fade col-md-4 m-b-2">
			<div class="img-thumbnail img-fluid text-xs-center" style="width:120px;height:120px;display:block;padding-top:40px;" >
				<img height="35" src="{{ asset('img/spinner.gif') }}" />
			</div>
		</div>

{% } %}
</script>

<script type="text/javascript" src="{{ asset('js/bootstrap-fileinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.money.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.fileupload.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		    $('#fileupload').fileupload({
			   	autoUpload:true,
			   	maxNumberOfFiles: 6,
        		url: '<?php echo url('/me/shop/products/photo') ;?>',
        		acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
    		});
    		
    		$('.money').maskMoney();
    		
    		$('#fileupload')
		    .bind('fileuploadstart', function (e) {
			    $('#submitBtn').attr('disabled', true).text('Uploading...');
		    })
				
		    .bind('fileuploadstop', function (e, data) {
		    	$('#submitBtn').attr('disabled', false).text('Save');
		    });
		    
		    function slug(str){
				str = str.replace(/^\s+|\s+$/g, ''); // trim
				str = str.toLowerCase();
				
				// remove accents, swap ñ for n, etc
				var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
				var to   = "aaaaeeeeiiiioooouuuunc------";
				
				for (var i=0, l=from.length ; i<l ; i++)
				str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
				
				
				str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
				.replace(/\s+/g, '-') // collapse whitespace and replace by -
				.replace(/-+/g, '-'); // collapse dashes
				
				return str;
		    }
		    
		    $('#name').keyup(function(){
			   $('#slug').val(slug($(this).val())); 
		    });

	});
</script>

@endsection

