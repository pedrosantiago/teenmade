@extends('layout')

@section('content')
<style>
	.slider-nav-thumbnails .slick-slide {
	opacity: 0.5;
	cursor:pointer;
}

.slider-nav-thumbnails .slick-slide.slick-current, .slider-nav-thumbnails .slick-slide:hover {
	opacity: 1;

}



</style>
<div class="container">
	
		<!-- wishlist -->
		
		
		
		<div class="row">
        <div class="col-md-5">
	        <div class="slider">
				@foreach($product->photos as $photo)
					<div><img class="img-rounded" width="100%" src="{{ asset('img/large/'.$photo->photo) }}" /></div>
				@endforeach	
			</div>
			<div class="slider-nav-thumbnails m-t-1">
				@foreach($product->photos as $photo)
					<div class="m-r-1"><img class="img-rounded img-fluid"  src="{{ asset('img/small/'.$photo->photo) }}" /></div>
				@endforeach	
			</div>
			
        </div>
        <div class="col-md-7 no-padding-left">
            <div class="single_product_details">
                <div class="single_product_description">
                    <h1 class="m-a-0">{{ $product->name }}</h1>
                    <h6 class="m-b-2">by <a href="{{ url($product->shop->slug) }}">{{ $product->user->name }}</a></h6>
                    <div class="star">

                            <i class="mdi mdi-star"></i>
                            <i class="mdi mdi-star"></i>
                            <i class="mdi mdi-star"></i>
                            <i class="mdi mdi-star-outline"></i>
                            <i class="mdi mdi-star-outline"></i>

                    </div>
                    
                    <hr />

                                            <p class="lead">
                            {{ $product->description }}
                        </p>
                        
                        
                        <p class="display-4 lead text-success" style="font-size:30px;">
                        	${{ money($product->price) }}
	                        @if($product->shipping == 0)
								<em class="small text-muted">with <strong>Free Shipping</strong></em>
							@else
								<em class="small text-muted">(+ ${{ money($product->shipping)}} shipping)</em>
							@endif
                        </p>
                        <br>
                        
						<nav class="nav nav-inline">
							<a class="nav-link disabled" href="#"><i class="mdi mdi-cube"></i> {{ $product->inventory }} units in Stock. </a>
							@if($product->user->is_verified)
							<a class="nav-link disabled" href="#"><i class="mdi mdi-check-circle"></i> Verified Seller</a>
							@endif
							
							@if(!$product->wishlist->contains(Auth::id()))
								<a class="nav-link" href="{{ url('wishlist/add', $product->product_id) }}"><i class="mdi mdi-heart"></i> Add to Wishlist</a>
							@else 
								<a class="nav-link" href="{{ url('wishlist/remove', $product->product_id) }}"><i class="mdi mdi-heart-broken"></i> Remove from Wishlist</a>
							@endif
							@if(Auth::id() == $product->user_id)
								<a class="nav-link" href="{{ url('me/shop/products/'.$product->product_id.'/edit') }}"><i class="mdi mdi-pencil"></i> Edit</a>
							@endif
							
						</nav>
						<br /><br />
						
						
                    
						@if(Auth::id() == $product->user_id)
							<button type="button" disabled="disabled" class="btn btn-lg btn-primary pull-left">Add to Cart</button>
						@else
						
						{{ Form::open(['url' => 'cart'])}}
						<div class="input-group" style="width:230px;">
							{{ Form::hidden('product_id', $product->product_id) }}
							<input type="text" style="border-radius:4px;" name="quantity" value="1" class="text-xs-center form-control-lg form-control" placeholder="Amt">
					      <span class="input-group-btn">
					        <button class="btn btn-primary btn-lg" type="submit">Add to Cart</button>
					      </span>
					    </div>
						{{ Form::close() }}

						@endif
                    

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		$(document).ready(function(){
			$('.slider').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: false,
				asNavFor: '.slider-nav-thumbnails',
				
			});
			
			 $('.slider-nav-thumbnails').slick({
			 	slidesToShow: 4,
			 	slidesToScroll: 1,
			 	asNavFor: '.slider',
			 	dots: false,
			 	arrows:false,
			 	focusOnSelect: true
			 });
			
			
			 //remove active class from all thumbnail slides
			 $('.slider-nav-thumbnails .slick-slide:not(.slick-current)').removeClass('slick-active');
			
			 // On before slide change match active thumbnail to current slide
			 $('.slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
			 	$('.slider-nav-thumbnails .slick-slide:not(.slick-current)').removeClass('slick-active');
			});
		});
	</script>
@endsection
