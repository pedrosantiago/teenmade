<table class="table table-curved">
			<thead>
				<tr>
					<th data-field="id">Product Name</th>
					<th data-field="name">Category</th>
					<th data-field="price">Price</th>
					<th data-field="inventory">Inventory</th>
					<th data-field="actions">Actions</th>
				</tr>
			</thead>
			<tbody>
            @foreach($shop->products as $product)
			<tr>
				<td>{{ $product->name }}</td>
				<td>{{ $product->category->name }}</td>
				<td>{{ '$'.money($product->price) }}</td>
				<td>{{ $product->inventory }}</td>
				<td class="p-a-0">
					<a class="btn btn-secondary btn-sm pull-left" style="margin-top:9px;margin-left:5px;" href="{{ url('me/shop/products/'.$product->product_id.'/edit') }}">Edit</a>
					
					{{ Form::open(['url' => 'me/shop/products/'.$product->product_id, 'method' => 'delete']) }}
						<button style="margin-top:9px;margin-right:5px;" class="btn btn-secondary btn-sm pull-right">Delete</button>
					{{ Form::close() }}
					
				</td>
			</tr>
			@endforeach
			</tbody>
		</table>