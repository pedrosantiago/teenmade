<div class="col-md-3">
	<div class="card">	
		<img class="card-img-top" src="{{ asset('img/medium/'.$product->photos->first()->photo) }}" style=" width: 100%; display: block;" alt="{{ $product->name }}">		
		<div class="card-block ">
			<h4 class="card-title">{{ $product->name }}</h4>
			<p class="card-text" style="font-size:1.1em;">
				${{ money($product->price) }}
				
				{{--
				@if($product->shipping == 0)
					<em class="small text-muted">with <strong>Free Shipping</strong></em>
				@else
					<em class="small text-muted">(+ ${{ money($product->shipping)}} shipping)</em>
				@endif
				--}}
				
			</p>
		</div>
		<div class="card-footer text-muted">
			{{ Form::open(['url' => 'cart'])}}
				{{ Form::hidden('quantity', 1) }}
				{{ Form::hidden('product_id', $product->product_id) }}
				<div class="btn-group btn-flex " role="group" aria-label="Basic example">
					<a href="{{ url($product->shop->slug.'/'.$product->slug) }}" role="button" style="opacity:.8" class="btn text-xs-center  btn-primary">View</a>
		
					<button type="submit" class="btn btn-primary"><i class="mdi mdi-cart-plus"> </i></button>
				</div>
			{{ Form::close() }}
		</div>
	</div>
</div>