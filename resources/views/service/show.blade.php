@extends('layout')

@section('content')
<div class="container">
	<h4 class="text-xs-center p-b-0 m-b-0">Teen Made</h4>
	<h1 class="display-2 text-xs-center" style="font-weight:200;">{{ $service->title }}</h1>
	<h5 class="text-xs-center"><a  href="#start-project" data-toggle="modal" class="text-uppercase ">Start {{ $service->title }} project.</a></h5>
	
	<hr />
	<br>
	
	{!! $service->description !!}
	
	
	
</div>

<!-- Modal -->
<div class="modal fade" id="start-project" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Start {{ $service->name }} Project</h4>
			</div>
			{{ Form::open(['url' => 'services/'.$service->service_id.'/quote'])}}
			<div class="modal-body">
				<div class="row">
					<fieldset class="form-group col-md-4">
						<label for="name">Name</label>
						{{ Form::text('name',  \Auth::check() ? \Auth::user()->name : null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Your name, please.']) }}
						<small class="text-muted">{{ $errors->first('name') }}</small>
					</fieldset>
					<fieldset class="form-group col-md-8">
						<label for="email">Email</label>
						{{ Form::text('email', \Auth::check() ? \Auth::user()->email : null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Enter e-mail']) }}
						<small class="text-muted">{{ $errors->first('email') }}</small>
					</fieldset>
				</div>
				<div class="row">
					<fieldset class="form-group col-md-6">
						<label for="name">Company</label>
						{{ Form::text('company', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Is it for a company?']) }}
						<small class="text-muted">{{ $errors->first('company') }}</small>
					</fieldset>
					<fieldset class="form-group col-md-6">
						<label for="name">Phone</label>
						{{ Form::text('phone', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'We will call if necessary.']) }}
						<small class="text-muted">{{ $errors->first('phone') }}</small>
					</fieldset>
				</div>
				<fieldset class="form-group">
					<label for="name">Project Description</label>
					{{ Form::textarea('description', null, ['id' => 'name', 'class' => 'form-control', 'rows' => 4]) }}
					<small class="text-muted">{{ $errors->first('description') }}</small>
				</fieldset>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Get Started</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection
@section('js')


@endsection
