@extends('layout')
@section('content')
	<div class="container">
		<h1>Services</h1>
		
		<div class="row">
			@each('service._card', $services, 'service')
		</div>
		
	</div>
@endsection