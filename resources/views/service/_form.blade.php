<div class="row">
	<fieldset class="form-group col-md-8">
		<label for="name">Title</label>
		{{ Form::text('title', null, ['id' => 'title', 'class' => 'form-control', 'placeholder' => 'Your name, please.']) }}
		<small class="text-muted">{{ $errors->first('title') }}</small>
	</fieldset>
	<fieldset class="form-group col-md-4">
		<label for="email">Background</label>
		{{ Form::text('color', null, ['id' => 'color', 'class' => 'form-control', 'placeholder' => 'Must be HEX']) }}
		<small class="text-muted">{{ $errors->first('color') }}</small>
	</fieldset>
</div>

<fieldset class="form-group">
	<label for="email">Description</label>
	{{ Form::textarea('description', null, ['rows' => 8, 'id' => 'description', 'class' => 'form-control', 'placeholder' => 'HTML is Allowed ;)']) }}
	<small class="text-muted">{{ $errors->first('description') }}</small>
</fieldset>
<fieldset class="form-group">
	<label for="email">Photo</label>
	{{ Form::file('photo', ['id' => 'photo', 'class' => 'form-control']) }}
	<small class="text-muted">{{ $errors->first('photo') }}</small>
</fieldset>