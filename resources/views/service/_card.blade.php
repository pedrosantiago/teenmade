<a href="{{ url('services', $service->service_id) }}" class="col-md-4">
	<div class="card card-inverse" style="overflow:hidden;height:200px;background-color:#{{ $service->color }}">
		@if($service->photo != null)
			<img class="card-img" src="{{ asset('img/other/'.$service->photo) }}" width="100%" style="" alt="TeenMade {{ $service->title }} service.">
		@endif
		<div class="card-img-overlay">
			<h3 class="card-title" style="margin-top:65px;text-align:center;">{{ $service->title }}</h3>
		</div>
	</div>
</a>