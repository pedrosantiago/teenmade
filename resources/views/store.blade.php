@extends('layout')

@section('content')
<div class="container">
	<div class="row">
		<div class="col s12">
			<div id="shop-panel" class="card-panel">
				<div class="shop-image-container">
					<div class="shop-image" style="background-image: url('{{ asset('img/cover/'.$shop->cover_picture)}}');"></div>
				</div>
				<div class="shop-panel-detail">
					<img src="{{ asset('img/logo/'.$shop->logo) }}" alt="" class="shop-avatar">
					<h2 class="">{{ $shop->name }}</h2>
					<h3>{{ $shop->headline }}</h3>
					<p class="shop-by-for">by <a href="{{ url($user->slug) }}">{{ $user->name }}</a></p>
					<p class="main-normal">{{ $shop->description }}</p>
				</div>
			</div>
		</div>
	</div>
	</div>
		@if(count($shop->products) > 0)
			<div class="row">
				@each('product._card', $products, 'product')
			</div>
		@else
			<h1 style="text-align:center;margin-top:150px;">Oh no =/</h1>
			<h2 style="text-align:center;">This user doesn't have any products, yet!</h2>
		@endif
</div>
@endsection