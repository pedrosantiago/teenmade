<table class="table table-curved">
	<thead>
		<tr>
			<th width="40%">Product</th>
			<th class="text-xs-center">Price</th>
			<th class="text-xs-center">Shipping</th>
			<th class="text-xs-center" width="130">Quantity</th>
			<th class="text-xs-center">Subtotal</th>
			<th class="text-xs-center">Actions</th>
		</tr>
	</thead>
	
	<tbody>
	@foreach($products as $product)
		<tr>
			<td width="40%"><a href="{{ url($product->attributes['url']) }}">{{ $product->name }}</a></td>
			<td class="text-xs-center">${{ money($product->price) }}</td>
			
			<td class="text-xs-center">{{ $product->attributes['shipping'] == 0 ? 'FREE' : '$'.money($product->attributes['shipping']) }}</td>

			<td class="text-xs-center actions">{{ Form::text('quantity', $product->quantity, ['class' => 'input-sm form-control quantity', 'style' => 'margin-top:9px;text-align:center;', 'data-id'=> $product->id]) }}</td>
			<td class="text-xs-center subtotalPrice{{ $product->id }}">${{ money($product->getPriceSum() + $product->attributes['shipping']) }}</td>
			
			<td class="text-xs-center" style="padding:0;padding-top:9px;">
				<a href="{{ url('cart', $product->id) }}" class="btn btn-sm btn-secondary">Remove</button>
			</td>
		</tr>
	@endforeach
	</tbody>
</table>