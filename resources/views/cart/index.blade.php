@extends('layout')

@section('content')
<div class="container">
	<h1>My Cart</h1>
	
	@if(Cart::isEmpty())
	<div class="jumbotron">
		<h1>Empty Cart</h1>
		<p class="lead">Browse a little bit more in teens stores and add some products to your cart.</p>
		<hr class="m-y-2" />
		<a class="btn btn-primary btn-lg" href="{{ url('/') }}" role="button">View Products</a>
	</div>
	@else
	@include('cart._table')
	<hr />
	<a href="{{ url('/checkout') }}" class="pull-right btn btn-lg btn-primary">Checkout</a>
	@endif
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('js/jquery.touchspin.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("input.quantity").TouchSpin().on("touchspin.on.stopspin", function(e) {
				id = $(this).data('id');
				$.post('<?php echo url('cart');?>/'+id, { quantity: $(this).val(), _token: '<?php echo csrf_token();?>', }, function(res){
					new NotificationFx({
						message : '<i class="mdi mdi-check"></i><p>'+res.message+'</p>',
						layout : 'bar',
						effect : 'slidetop',
						type : 'notice',
						ttl: 1000
					}).show();
					
					$('.subtotalPrice'+id).html(res.price);
							
				});
				
			});
		});
	</script>
@endsection